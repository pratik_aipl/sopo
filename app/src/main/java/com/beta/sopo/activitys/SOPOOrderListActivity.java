package com.beta.sopo.activitys;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.beta.sopo.BaseActivity;
import com.beta.sopo.R;
import com.beta.sopo.adpater.OrderListAdpater;
import com.beta.sopo.listner.SearchDataList;
import com.beta.sopo.model.OrderSOPO;
import com.beta.sopo.network.NetworkRequest;
import com.beta.sopo.utils.Constant;
import com.beta.sopo.utils.L;
import com.bluelinelabs.logansquare.LoganSquare;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class SOPOOrderListActivity extends BaseActivity implements SearchDataList {

    OrderListAdpater orderListAdpater;
    String From, Back;

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.img_edit)
    ImageView imgEdit;
    @BindView(R.id.img_addd)
    ImageView imgAddd;
    @BindView(R.id.lli)
    LinearLayout lli;
    @BindView(R.id.lin_header)
    LinearLayout linHeader;
    @BindView(R.id.tv_tittle2)
    TextView tvTittle2;
    @BindView(R.id.list)
    LinearLayout list;
    @BindView(R.id.recycler_list)
    RecyclerView OrderList;

    Subscription subscription;
    private static final String TAG = "SOPOOrderListActivity";

    List<OrderSOPO> Ordersopo = new ArrayList<>();

    Dialog FilterDialog;
    String StatusName;
    @BindView(R.id.tv_found)
    TextView tvFound;
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.activity_sopoorder_list);
        ButterKnife.bind(this);

        imgAddd.setVisibility(View.VISIBLE);
        imgAddd.setImageResource(R.drawable.filter);

        if (getIntent() != null)
            From = getIntent().getStringExtra("FROM");
        Back = getIntent().getStringExtra("Back");
        if (From.equalsIgnoreCase("SO")) {
            tvTittle.setText("SO ORDERS");
            tvTittle2.setText("SO ORDERS LIST");

        } else {
            tvTittle.setText("PO ORDERS");
            tvTittle2.setText("PO ORDERS LIST");
        }
        if (L.isNetworkAvailable(SOPOOrderListActivity.this))
            getSOPOList();


        swipeContainer.setOnRefreshListener(() -> {
            getSOPOList();
            swipeContainer.setRefreshing(false);
        });
        OrderList.setHasFixedSize(true);
        OrderList.setItemAnimator(new DefaultItemAnimator());
        OrderList.setLayoutManager(new LinearLayoutManager(this));
      /*  orderListAdpater = new OrderListAdpater(this, Ordersopo, From);
        OrderList.setAdapter(orderListAdpater);*/
        setListData();

    }

    @OnClick({R.id.img_back, R.id.img_addd})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                if (Back != null && Back.equalsIgnoreCase("Dash")) {
                    startActivity(new Intent(SOPOOrderListActivity.this, DashboardActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                    Intent.FLAG_ACTIVITY_SINGLE_TOP |
                                    Intent.FLAG_ACTIVITY_CLEAR_TOP));

                } else {
                    onBackPressed();
                }
                break;
            case R.id.img_addd:
                AddMDialog();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (Back != null && Back.equalsIgnoreCase("Dash")) {
            startActivity(new Intent(SOPOOrderListActivity.this, DashboardActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                            Intent.FLAG_ACTIVITY_CLEAR_TASK |
                            Intent.FLAG_ACTIVITY_SINGLE_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TOP));

        } else {
            super.onBackPressed();
        }

    }

    public void AddMDialog() {

        FilterDialog = new Dialog(this, R.style.Theme_Dialog);
        FilterDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        FilterDialog.setCancelable(false);
        FilterDialog.setContentView(R.layout.filter_item);

        String[] Status = new String[]{"All", "Pending", "Approved", "Rejected"};

        Button btnclose, btn_filter, btn_reset;
        EditText edt_compnyname, edt_customername;
        Spinner spn_status;
        ImageView img_close;


        btnclose = FilterDialog.findViewById(R.id.btn_cancel);
        btn_reset = FilterDialog.findViewById(R.id.btn_reset);
        btn_filter = FilterDialog.findViewById(R.id.btn_filter);
        img_close = FilterDialog.findViewById(R.id.img_close);

        edt_compnyname = FilterDialog.findViewById(R.id.edt_compnyname);
        edt_customername = FilterDialog.findViewById(R.id.edt_customername);
        spn_status = FilterDialog.findViewById(R.id.spn_status);


        if (From.equalsIgnoreCase("SO")) {
            edt_customername.setHint("Customer Name");
            if (prefs != null) {
                edt_compnyname.setText(prefs.getString(Constant.SOCompanyName, null));
                edt_customername.setText(prefs.getString(Constant.SOCustomerName, null));
            }

        } else {
            edt_customername.setHint("Supplier Name");
            if (prefs != null) {
                edt_compnyname.setText(prefs.getString(Constant.POCompanyName, null));
                edt_customername.setText(prefs.getString(Constant.POCustomerName, null));
            }
        }


        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this, android.R.layout.select_dialog_item, Status);
        spn_status.setAdapter(adapter);

        spn_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                StatusName = String.valueOf(spn_status.getAdapter().getItem(position));
                Log.d(TAG, "onItemSelected: " + StatusName);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btnclose.setOnClickListener(v -> {
            if (From.equalsIgnoreCase("SO")) {
                prefs.save(Constant.SOCompanyName, "");
                prefs.save(Constant.SOCustomerName, "");

            } else {
                prefs.save(Constant.POCompanyName, "");
                prefs.save(Constant.POCustomerName, "");
            }
            edt_compnyname.setText("");
            edt_customername.setText("");
            spn_status.setAdapter(adapter);
        });

        img_close.setOnClickListener(v -> FilterDialog.dismiss());


        btn_filter.setOnClickListener(v -> {

          /*  if (TextUtils.isEmpty(L.getEditText(edt_compnyname))) {
                edt_compnyname.setError("Please enter Company Name");
            } else if (TextUtils.isEmpty(L.getEditText(edt_customername))) {
                edt_customername.setError("Please enter Customer Name");
            } else if (StatusName == null) {
                Toast.makeText(this, "Please Select Status", Toast.LENGTH_SHORT).show();
            } else {*/
            if (StatusName.equalsIgnoreCase("All")
                    && TextUtils.isEmpty(edt_compnyname.getText().toString())
                    && TextUtils.isEmpty(edt_customername.getText().toString())) {
                setListData();
            } else {
                orderListAdpater.getFilter().filter(edt_compnyname.getText().toString().trim()
                        + "," + edt_customername.getText().toString().trim()
                        + "," + StatusName);
            }

            // Log.d(TAG, "AddMDialog: " + new Gson().toJson(AddsopoMaterials));

            if (From.equalsIgnoreCase("SO")) {
                prefs.save(Constant.SOCompanyName, edt_compnyname.getText().toString().trim());
                prefs.save(Constant.SOCustomerName, edt_customername.getText().toString().trim());

            } else {
                prefs.save(Constant.POCompanyName, edt_compnyname.getText().toString().trim());
                prefs.save(Constant.POCustomerName, edt_customername.getText().toString().trim());
            }


            FilterDialog.dismiss();

            // }
        });

        btn_reset.setOnClickListener(v -> {
            //  orderListAdpater.getFilter().filter("" + "," + "" + "," + "");
            setListData();
            if (From.equalsIgnoreCase("SO")) {
                prefs.save(Constant.SOCompanyName, "");
                prefs.save(Constant.SOCustomerName, "");

            } else {
                prefs.save(Constant.POCompanyName, "");
                prefs.save(Constant.POCustomerName, "");
            }
            edt_compnyname.setText("");
            edt_customername.setText("");

            FilterDialog.dismiss();

        });


        FilterDialog.show();

    }

    public void setListData() {
        if (Ordersopo.size() == 0) {
            tvFound.setVisibility(View.VISIBLE);
        } else {
            tvFound.setVisibility(View.GONE);
        }
        orderListAdpater = new OrderListAdpater(this, Ordersopo, From);
        OrderList.setAdapter(orderListAdpater);
    }

    private void getSOPOList() {
        Map<String, String> map = new HashMap<>();
        map.put("OrderID", "0");
        map.put("UserID", user.getUserID());
        map.put("OrderType", From);
        map.put("FromDate", "");
        map.put("ToDate", "");

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getOrderSOPO(From.equalsIgnoreCase("SO") ? "AllSO" : "AllPO", map), (data) -> {
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.getBoolean("Status")) {
                        // Toast.makeText(this, jsonResponse.getString("UMessage"), Toast.LENGTH_SHORT).show();
                        if (jsonResponse.getString(Constant.sopoList).equalsIgnoreCase("null")) {
                            tvFound.setVisibility(View.VISIBLE);
                            imgAddd.setVisibility(View.GONE);
                        } else {
                            tvFound.setVisibility(View.GONE);
                            imgAddd.setVisibility(View.VISIBLE);
                            Ordersopo.clear();
                            Ordersopo.addAll(LoganSquare.parseList(jsonResponse.getJSONArray(Constant.sopoList).toString(), OrderSOPO.class));
                            orderListAdpater.notifyDataSetChanged();
                        }
                    } else {
                        L.alert(SOPOOrderListActivity.this, jsonResponse.getString("UMessage"));
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                showProgress(false);
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    @Override
    public void onListsize(int Listsize) {
        if (Listsize == 0) {
            tvFound.setVisibility(View.VISIBLE);
        } else {
            tvFound.setVisibility(View.GONE);
        }
    }
}
