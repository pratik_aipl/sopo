package com.beta.sopo.activitys;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.beta.sopo.BaseActivity;
import com.beta.sopo.R;
import com.beta.sopo.adpater.MaterialListAdpater;
import com.beta.sopo.model.OrderSOPODetails;
import com.beta.sopo.model.SOPOMaterial;
import com.beta.sopo.network.NetworkRequest;
import com.beta.sopo.utils.Constant;
import com.beta.sopo.utils.L;
import com.bluelinelabs.logansquare.LoganSquare;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class OrderDetailsActivity extends BaseActivity {

    private static final String TAG = "OrderDetailsActivity";
    MaterialListAdpater materialListAdpater;

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.img_edit)
    ImageView imgEdit;
    @BindView(R.id.img_addd)
    ImageView imgAddd;
    @BindView(R.id.lli)
    LinearLayout lli;
    @BindView(R.id.recycler_list)
    RecyclerView OrderDetailList;
    String Type;
    int OrderID;

    List<SOPOMaterial> sopoMaterials = new ArrayList<>();

    Subscription subscription;
    @BindView(R.id.tv_Dsupplier)
    TextView tvDsupplier;
    @BindView(R.id.tv_Dstatus)
    TextView tvDstatus;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.tv_OrderNO)
    TextView tvOrderNO;
    @BindView(R.id.tv_DTamount)
    TextView tvDTamount;
    @BindView(R.id.tv_Division)
    TextView tvDivision;
    @BindView(R.id.tv_SubDivision)
    TextView tvSubDivision;
    @BindView(R.id.tv_PTerm)
    TextView tvPTerm;
    @BindView(R.id.tv_DTerm)
    TextView tvDTerm;
    @BindView(R.id.tv_Remark)
    TextView tvRemark;

    OrderSOPODetails orderSOPODetails = new OrderSOPODetails();
    @BindView(R.id.tv_LoadingType)
    TextView tvLoadingType;
    @BindView(R.id.tv_CommissionAgent)
    TextView tvCommissionAgent;
    @BindView(R.id.tv_soponame)
    TextView tvSoponame;
    @BindView(R.id.lin_lodingtype)
    LinearLayout linLodingtype;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        ButterKnife.bind(this);


        if (getIntent() != null) {
            OrderID = getIntent().getIntExtra("ID", 0);
            Type = getIntent().getStringExtra("Type");
        }
        if (L.isNetworkAvailable(OrderDetailsActivity.this))
            getOrderSOPODetails();

        OrderDetailList.setHasFixedSize(true);
        OrderDetailList.setItemAnimator(new DefaultItemAnimator());
        OrderDetailList.setLayoutManager(new LinearLayoutManager(this));


    }

    @OnClick({R.id.img_back, R.id.img_edit, R.id.img_addd})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.img_edit:
                startActivity(new Intent(OrderDetailsActivity.this, SOPOOrderActivity.class)
                        .putExtra(Constant.Type, "Edit")
                        .putExtra("FROM", Type)
                        .putExtra(Constant.OrderSOPODetails, orderSOPODetails)
                        .putExtra(Constant.sopoMaterial, (Serializable) sopoMaterials));
                break;
            case R.id.img_addd:
                alert();
                break;
        }
    }

    public void alert() {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to delete this " + Type + " ?")
                .setPositiveButton("YES", (dialog, which) -> {
                    DeleteOrder();

                })
                .setNegativeButton("NO", null)
                .show();
    }

    public void Setdata() {
        tvTittle.setText(orderSOPODetails.getCompanyName());
        tvDsupplier.setText(orderSOPODetails.getPartyName());
        tvDstatus.setText(orderSOPODetails.getOrderStatus());
        tvDate.setText(orderSOPODetails.getOrderDate());
        tvOrderNO.setText(orderSOPODetails.getOrderNo());
        tvDTamount.setText(orderSOPODetails.getCurrencyName() + " " + orderSOPODetails.getTotalOrderAmount());

        tvDivision.setText(orderSOPODetails.getDivisionName());
        tvSubDivision.setText(orderSOPODetails.getSubDivisionName());
        tvPTerm.setText(orderSOPODetails.getPaymentTerm());
        tvDTerm.setText(orderSOPODetails.getDeliveryTerm());
        tvRemark.setText(orderSOPODetails.getRemark());
        tvLoadingType.setText(orderSOPODetails.getLoadingType());
        tvCommissionAgent.setText(orderSOPODetails.getCommGroupName());

        if (!orderSOPODetails.getOrderStatus().equalsIgnoreCase("Pending")) {
            imgAddd.setVisibility(View.GONE);
            imgEdit.setVisibility(View.GONE);
        } else {
            imgEdit.setVisibility(View.VISIBLE);
            imgEdit.setImageResource(R.drawable.editw);

            imgAddd.setVisibility(View.VISIBLE);
            imgAddd.setImageResource(R.drawable.deletew);
        }

        if (Type.equalsIgnoreCase("SO")) {
            tvSoponame.setText("Customer Name");
            linLodingtype.setVisibility(View.GONE);
        } else {
            tvSoponame.setText("Supplier Name");
            linLodingtype.setVisibility(View.VISIBLE);
        }


    }

    private void getOrderSOPODetails() {
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getOrderSOPODetails(Type.equalsIgnoreCase("SO") ? "SOByID" : "POByID", OrderID), (data) -> {
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.getBoolean("Status")) {
                        //Toast.makeText(this, jsonResponse.getString("UMessage"), Toast.LENGTH_SHORT).show();

                        //Get Order Details
                        orderSOPODetails = LoganSquare.parse(jsonResponse.getJSONObject(Constant.orders).toString(), OrderSOPODetails.class);

                        //Get Material List
                        sopoMaterials.addAll(LoganSquare.parseList(jsonResponse.getJSONArray(Constant.material).toString(), SOPOMaterial.class));

                        materialListAdpater = new MaterialListAdpater(this, sopoMaterials, "DETAIL");
                        OrderDetailList.setAdapter(materialListAdpater);

                        Setdata();

                    } else {
                        L.alert(OrderDetailsActivity.this, jsonResponse.getString("UMessage"));

                        //Toast.makeText(this, jsonResponse.getString("UMessage"), Toast.LENGTH_SHORT).show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                showProgress(false);

            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    private void DeleteOrder() {
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.DeleteOrder(Type.equalsIgnoreCase("SO") ? "DeleteSO" : "DeletePO", OrderID), (data) -> {
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.getBoolean("Status")) {

                        new AlertDialog.Builder(this)
                                .setMessage(jsonResponse.getString("UMessage"))
                                .setPositiveButton("OK", (dialog, which) -> {

                                    startActivity(new Intent(OrderDetailsActivity.this, SOPOOrderListActivity.class)
                                            .putExtra("FROM", Type)
                                            .putExtra("Back", "Dash")
                                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                                                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                                    Intent.FLAG_ACTIVITY_SINGLE_TOP |
                                                    Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                })
                                .show();


                    } else {
                        L.alert(OrderDetailsActivity.this, jsonResponse.getString("UMessage"));

                        //Toast.makeText(this, jsonResponse.getString("UMessage"), Toast.LENGTH_SHORT).show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                showProgress(false);

            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

}
