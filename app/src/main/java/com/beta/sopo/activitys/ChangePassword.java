package com.beta.sopo.activitys;

import android.os.Bundle;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.beta.sopo.BaseActivity;
import com.beta.sopo.R;
import com.beta.sopo.network.NetworkRequest;
import com.beta.sopo.utils.L;
import com.beta.sopo.utils.Validation;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class ChangePassword extends BaseActivity {


    @BindView(R.id.btn_send)
    Button btnSend;
    @BindView(R.id.img_back)
    ImageView imgBack;

    Subscription subscription;
    @BindView(R.id.edt_old_paswd)
    EditText edtOldPaswd;
    @BindView(R.id.edt_new_paswd)
    EditText edtNewPaswd;
    @BindView(R.id.edt_cnf_paswd)
    EditText edtCnfPaswd;
    @BindView(R.id.img_old_show)
    ImageView imgOldShow;
    @BindView(R.id.img_new_show)
    ImageView imgNewShow;
    @BindView(R.id.img_cnf_show)
    ImageView imgCnfShow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        imgOldShow.setOnTouchListener((v, event) -> {

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    edtOldPaswd.setInputType(InputType.TYPE_CLASS_TEXT);
                    break;
                case MotionEvent.ACTION_UP:
                    edtOldPaswd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    break;
            }
            return true;
        });
        imgNewShow.setOnTouchListener((v, event) -> {

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    edtNewPaswd.setInputType(InputType.TYPE_CLASS_TEXT);
                    break;
                case MotionEvent.ACTION_UP:
                    edtNewPaswd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    break;
            }
            return true;
        });
        imgCnfShow.setOnTouchListener((v, event) -> {

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    edtCnfPaswd.setInputType(InputType.TYPE_CLASS_TEXT);
                    break;
                case MotionEvent.ACTION_UP:
                    edtCnfPaswd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    break;
            }
            return true;
        });
    }


    @OnClick({R.id.img_back, R.id.btn_send})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.btn_send:
                validation();
                break;
        }
    }

    public void validation() {
        if (Validation.isEmpty(L.getEditText(edtOldPaswd))) {
            edtOldPaswd.setError("Please enter OLD Password");
        } else if ((L.getEditText(edtOldPaswd).length() < 8)) {
            edtOldPaswd.setError("Please enter Minimum 8 Character");
        } else if (Validation.isEmpty(L.getEditText(edtNewPaswd))) {
            edtNewPaswd.setError("Please enter NEW Password");
        } else if ((L.getEditText(edtNewPaswd).length() < 8)) {
            edtNewPaswd.setError("Please enter Minimum 8 Character");
        } else if (Validation.isEmpty(L.getEditText(edtCnfPaswd))) {
            edtCnfPaswd.setError("Please enter Confirm Password");
        } else if ((L.getEditText(edtCnfPaswd).length() < 8)) {
            edtCnfPaswd.setError("Please enter Minimum 8 Character");
        } else if (!L.getEditText(edtNewPaswd).matches(L.getEditText(edtCnfPaswd))) {
            Toast.makeText(this, "Please Enter Same Password", Toast.LENGTH_SHORT).show();
        } else {
            if (L.isNetworkAvailable(ChangePassword.this)) {

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("UserID", user.getUserID());
                    jsonObject.put("OldPassword", edtOldPaswd.getText().toString().trim());
                    jsonObject.put("NewPassword", edtNewPaswd.getText().toString().trim());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ChangePaswd(jsonObject.toString());
            }
        }
    }

    private void ChangePaswd(String s) {
  /*      Map<String, String> map = new HashMap<>();
        map.put("UserID", user.getUserID());
        map.put("OldPassword", edtOldPaswd.getText().toString().trim());
        map.put("NewPassword", edtNewPaswd.getText().toString().trim());*/

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.ChangePassword(s), (data) -> {
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.getBoolean("Status")) {
                        Toast.makeText(this, jsonResponse.getString("UMessage"), Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    } else {
                        L.alert(ChangePassword.this, jsonResponse.getString("UMessage"));
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                showProgress(false);
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }
}
