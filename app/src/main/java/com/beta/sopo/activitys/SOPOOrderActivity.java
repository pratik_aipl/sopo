package com.beta.sopo.activitys;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.beta.sopo.BaseActivity;
import com.beta.sopo.R;
import com.beta.sopo.model.AllCountry;
import com.beta.sopo.model.CommissionAgentModel;
import com.beta.sopo.model.DTPTCItemModel;
import com.beta.sopo.model.DTPTCMainModel;
import com.beta.sopo.model.DivisionModel;
import com.beta.sopo.model.MainDataModel;
import com.beta.sopo.model.OrderEntity;
import com.beta.sopo.model.OrderSOPODetails;
import com.beta.sopo.model.SOPOMaterial;
import com.beta.sopo.network.NetworkRequest;
import com.beta.sopo.utils.Constant;
import com.beta.sopo.utils.L;
import com.beta.sopo.utils.Validation;
import com.bluelinelabs.logansquare.LoganSquare;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class SOPOOrderActivity extends BaseActivity {

    private static final String TAG = "SOPOOrderActivity";
    String From, Type, OrderType;

    Dialog profiledialog;
    DatePickerDialog datePickerDialog;
    final Calendar myCalendar = Calendar.getInstance();

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tittle;
    @BindView(R.id.img_edit)
    ImageView imgEdit;
    @BindView(R.id.img_addd)
    ImageView imgAddd;
    @BindView(R.id.lli)
    LinearLayout lli;
    @BindView(R.id.tv_Dinvoice)
    TextView tvDinvoice;
    @BindView(R.id.ll_dob)
    RelativeLayout llDob;
    @BindView(R.id.spn_cmpny)
    Spinner spnCmpny;
    @BindView(R.id.li_compny)
    RelativeLayout liCompny;
    @BindView(R.id.spn_division)
    Spinner spnDivision;
    @BindView(R.id.li_division)
    RelativeLayout liDivision;
    @BindView(R.id.spn_sub_division)
    Spinner spnSubDivision;
    @BindView(R.id.li_sub_division)
    RelativeLayout liSubDivision;
    @BindView(R.id.aTv_supplier)
    AutoCompleteTextView Ac_supplier;
    @BindView(R.id.img_add)
    ImageView imgAdd;
    @BindView(R.id.li_supplier)
    LinearLayout liSupplier;
    @BindView(R.id.view_supplier)
    View viewSupplier;
    @BindView(R.id.aTv_Customer)
    AutoCompleteTextView AC_compny;
    @BindView(R.id.img_add_cust)
    ImageView imgAddCust;
    @BindView(R.id.li_customer)
    LinearLayout liCustomer;
    @BindView(R.id.view_customer)
    View viewCustomer;
    @BindView(R.id.spn_Currency)
    Spinner spnCurrency;
    @BindView(R.id.li_currency)
    RelativeLayout liCurrency;
    @BindView(R.id.spn_PTerm)
    Spinner spnPTerm;
    @BindView(R.id.li_Paymentterm)
    RelativeLayout liPaymentterm;
    @BindView(R.id.spn_DTerm)
    Spinner spnDTerm;
    @BindView(R.id.li_Deliveryterm)
    RelativeLayout liDeliveryterm;
    @BindView(R.id.btn_next)
    Button btnNext;

    Subscription subscription;


    DTPTCMainModel dtptcMainModel;
    DivisionModel divisionModel;
    MainDataModel mainDataModel;
    CommissionAgentModel commissionagentmodel;

    OrderSOPODetails orderSOPODetails;

    List<DTPTCItemModel> CompanyList = new ArrayList<>();
    List<DTPTCItemModel> paymentTermsList = new ArrayList<>();
    List<DTPTCItemModel> deliveryTermsList = new ArrayList<>();
    List<DTPTCItemModel> currenciesList = new ArrayList<>();
    List<DTPTCItemModel> DivisionList = new ArrayList<>();

    public ArrayList<String> strSupplierArray = new ArrayList<>();
    public ArrayList<String> strCustomerArray = new ArrayList<>();
    public ArrayList<String> strCountryArray = new ArrayList<>();

    CompanyAdapter companyAdapter;
    PaymentTermsAdapter paymentTermsAdapter;
    DeliveryTermsAdapter deliveryTermsAdapter;
    CurrenciesAdapter currenciesAdapter;
    DivisionAdapter divisionAdapter;
    SubDivisionAdapter subdivisionAdapter;
    CommissionAgentAdapter commissionAgentAdapter;

    String CompanyID, CompanyIDGet, DivisionID, COMMISSIONAGENTID, SubDivisionID, CurrencyID, CurrencyType, PaymentTermID, DeliveryTermID, SupplierID, CustomerID, CountryID;
    @BindView(R.id.vi_division)
    View viDivision;
    @BindView(R.id.vi_subdivision)
    View viSubdivision;

    List<SOPOMaterial> sopoMaterials = new ArrayList<>();

    List<AllCountry> allCountry = new ArrayList<>();
    String CompanyType;

    OrderEntity orderEntitys;
    String getData;
    @BindView(R.id.rd_Direct)
    RadioButton rdDirect;
    @BindView(R.id.rd_Warehouse)
    RadioButton rdWarehouse;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.lin_dir_pur)
    LinearLayout linDirPur;
    @BindView(R.id.spn_CommissionAgent)
    Spinner spnCommissionAgent;
    @BindView(R.id.li_CommissionAgent)
    RelativeLayout liCommissionAgent;
    @BindView(R.id.vi_commision)
    View viCommision;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sopoorder);
        ButterKnife.bind(this);


        if (getIntent() != null) {
            From = getIntent().getStringExtra("FROM");
            Type = getIntent().getStringExtra(Constant.Type);
        }
        if (Type != null)
            if (Type.equalsIgnoreCase("Edit")) {
                prefs.save(Constant.OrderEntitySO, null);
                prefs.save(Constant.OrderEntityPO, null);
                prefs.save(Constant.MaterialDataSO, null);
                prefs.save(Constant.MaterialDataPO, null);

                orderSOPODetails = (OrderSOPODetails) getIntent().getSerializableExtra(Constant.OrderSOPODetails);
                sopoMaterials = (List<SOPOMaterial>) getIntent().getSerializableExtra(Constant.sopoMaterial);
            }


        if (From.equalsIgnoreCase("SO")) {
            tittle.setText("SO ORDERS");
            linDirPur.setVisibility(View.GONE);
            CompanyType = "CUSTOMER";
            getData = prefs.getString(Constant.OrderEntitySO, null);
        } else if (From.equalsIgnoreCase("PO")) {
            tittle.setText("PO ORDERS");
            linDirPur.setVisibility(View.VISIBLE);
            CompanyType = "SUPPLIER";
            getData = prefs.getString(Constant.OrderEntityPO, null);
        }


        if (Type != null) {
            OrderType = Type;
        } else {
            OrderType = "Add";
        }


        //Company List
        companyAdapter = new CompanyAdapter(this, CompanyList);
        spnCmpny.setAdapter(companyAdapter);

        paymentTermsAdapter = new PaymentTermsAdapter(this, paymentTermsList);
        spnPTerm.setAdapter(paymentTermsAdapter);

        deliveryTermsAdapter = new DeliveryTermsAdapter(this, deliveryTermsList);
        spnDTerm.setAdapter(deliveryTermsAdapter);

        currenciesAdapter = new CurrenciesAdapter(this, currenciesList);
        spnCurrency.setAdapter(currenciesAdapter);

        if (prefs != null && getData != null) {
            alert();
        } else {
            if (L.isNetworkAvailable(this))
                getDTPTC();
        }


        spnCmpny.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    CompanyIDGet = CompanyList.get(position - 1).getUCID();
                    CompanyID = CompanyList.get(position - 1).getRecID();
                    getDivision();

                } else {
                    liDivision.setVisibility(View.GONE);
                    viDivision.setVisibility(View.GONE);
                    liSubDivision.setVisibility(View.GONE);
                    viSubdivision.setVisibility(View.GONE);
                    liCustomer.setVisibility(View.GONE);
                    liSupplier.setVisibility(View.GONE);
                    SupplierID = null;
                    CustomerID = null;


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spnDivision.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    DivisionList.clear();
                    DivisionList.addAll(divisionModel.getDivisions());
                    DivisionID = DivisionList.get(position - 1).getRecID();
                    getSubDivision();

                    if (From.equalsIgnoreCase("SO")) {
                        // prefs.save(Constant.MaterialDataSO, null);

                        AC_compny.setText("");
                        CustomerID = null;
                        //  sopoMaterials.clear();

                    } else if (From.equalsIgnoreCase("PO")) {
                        // prefs.save(Constant.MaterialDataPO, null);
                        Ac_supplier.setText("");
                        SupplierID = null;
                        // sopoMaterials.clear();
                    }

                } else {
                    liSubDivision.setVisibility(View.GONE);
                    viSubdivision.setVisibility(View.GONE);
                    liCustomer.setVisibility(View.GONE);
                    liSupplier.setVisibility(View.GONE);
                    SupplierID = null;
                    CustomerID = null;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spnSubDivision.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    SubDivisionID = mainDataModel.getSubDivisions().get(position - 1).getRecID();
                    getAllCommissionAgent();
                } else {
                    liCommissionAgent.setVisibility(View.GONE);
                    viCommision.setVisibility(View.GONE);
                    COMMISSIONAGENTID = null;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spnCommissionAgent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    COMMISSIONAGENTID = commissionagentmodel.getCommissions().get(position - 1).getRecID();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spnCurrency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 0) {
                    CurrencyID = dtptcMainModel.getCurrencies().get(position - 1).getRecID();
                    CurrencyType = dtptcMainModel.getCurrencies().get(position - 1).getCurCode();

                }
                //   AC_compny.setFocusable(false);
                // Ac_supplier.setFocusable(false);

                Ac_supplier.setCursorVisible(false);
                AC_compny.setCursorVisible(false);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spnPTerm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    PaymentTermID = dtptcMainModel.getPaymentTerms().get(position - 1).getRecID();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spnDTerm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    DeliveryTermID = dtptcMainModel.getDeliveryTerms().get(position - 1).getRecID();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        Ac_supplier.setOnItemClickListener((parent, view, position, rowId) -> {

            String selection = (String) parent.getItemAtPosition(position);
            Log.d(TAG, "Selection: " + selection);

            for (int i = 0; i < mainDataModel.getSuppliers().size(); i++) {
                if (selection.equalsIgnoreCase(mainDataModel.getSuppliers().get(i).getCompanyName())) {
                    SupplierID = mainDataModel.getSuppliers().get(i).getRecID();
                    break;
                }
            }
            HideKeyboard();

        });
        AC_compny.setOnItemClickListener((parent, view, position, rowId) -> {
            String selection = (String) parent.getItemAtPosition(position);
            Log.d(TAG, "Selection: " + selection);

            for (int i = 0; i < mainDataModel.getCustomers().size(); i++) {
                if (selection.equalsIgnoreCase(mainDataModel.getCustomers().get(i).getCompanyName())) {
                    CustomerID = mainDataModel.getCustomers().get(i).getRecID();
                    break;
                }
            }
            HideKeyboard();

        });

        AC_compny.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                AC_compny.setCursorVisible(true);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                CustomerID = null;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        Ac_supplier.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Ac_supplier.setCursorVisible(true);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                SupplierID = null;
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public void HideKeyboard() {

        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }


    public void alert() {
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setMessage((R.string.OrderStore))

                .setPositiveButton("Continue", (dialog, which) -> {

                    orderEntitys = new Gson().fromJson(getData, OrderEntity.class);
                    Log.d(TAG, "prefs DATA>>>: " + getData);
                    if (L.isNetworkAvailable(this))
                        getDTPTC();
                })
                .setNegativeButton("Delete", (dialog, which) -> {
                    //  RealmController.with(this).clearAll();

                    if (From.equalsIgnoreCase("SO")) {
                        prefs.save(Constant.OrderEntitySO, null);
                        prefs.save(Constant.MaterialDataSO, null);
                    } else {
                        prefs.save(Constant.OrderEntityPO, null);
                        prefs.save(Constant.MaterialDataPO, null);

                    }
                    if (L.isNetworkAvailable(this))
                        getDTPTC();
                })
                .show();
    }

    private void toDate() {
        //String myFormat = "yyyy-MM-dd hh:mm:ss"; //In which you need put here
        String myFormat = "dd MMM yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        //tvToDate.setText(L.getDate(sdf.format(myCalendar.getTime())));
        tvDinvoice.setText(sdf.format(myCalendar.getTime()));
    }

    DatePickerDialog.OnDateSetListener todate = (view, year, monthOfYear, dayOfMonth) -> {
        // TODO Auto-generated method stub
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, monthOfYear);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        toDate();
    };

    public void AddDialog(Activity activity) {
        profiledialog = new Dialog(activity, R.style.Theme_Dialog);
        profiledialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        profiledialog.setCancelable(false);
        profiledialog.setContentView(R.layout.add_customer);
        Button btnclose, btnAdd;
        EditText edt_cpmny, edt_address, edt_city, edt_cont_per, edt_cont_no, edt_email;
        AutoCompleteTextView Ac_Country;

        btnAdd = profiledialog.findViewById(R.id.btn_Dadd);
        btnclose = profiledialog.findViewById(R.id.btn_cancel);

        Ac_Country = profiledialog.findViewById(R.id.aTv_Country);

        edt_cpmny = profiledialog.findViewById(R.id.edt_cpmny);
        edt_address = profiledialog.findViewById(R.id.edt_address);
        edt_city = profiledialog.findViewById(R.id.edt_city);
        edt_cont_per = profiledialog.findViewById(R.id.edt_cont_per);
        edt_cont_no = profiledialog.findViewById(R.id.edt_cont_no);
        edt_email = profiledialog.findViewById(R.id.edt_email);

        btnclose.setOnClickListener(v -> profiledialog.dismiss());


        if (L.isNetworkAvailable(this))
            getCountry();


        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this, android.R.layout.select_dialog_item, strCountryArray);
        Ac_Country.setThreshold(0);
        Ac_Country.setAdapter(adapter);
        Ac_Country.setTextColor(Color.BLACK);

        Ac_Country.setOnItemClickListener((parent, view, position, rowId) -> {
            String selection = (String) parent.getItemAtPosition(position);
            Log.d(TAG, "Selection: " + selection);

            for (int i = 0; i < allCountry.size(); i++) {
                if (selection.equalsIgnoreCase(allCountry.get(i).getCountryName())) {
                    CountryID = allCountry.get(i).getRecID();
                    break;
                }
            }


        });
        Ac_Country.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                CountryID = null;
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        btnAdd.setOnClickListener(v -> {

            if (Validation.isEmpty(L.getEditText(edt_cpmny))) {
                edt_cpmny.setError("Please enter Company Name");
            } else if (Validation.isEmpty(L.getEditText(edt_address))) {
                edt_address.setError("Please enter Address");
            } else if (Validation.isEmpty(L.getEditText(edt_city))) {
                edt_city.setError("Please enter City");
            } else if (Validation.isEmpty(L.getEditText(edt_cont_per))) {
                edt_cont_per.setError("Please enter Contact Person Name");
            } else if (Validation.isEmpty(L.getEditText(edt_cont_no))) {
                edt_cont_no.setError("Please enter Contact No.");
            } else if (!Validation.isValidEmail(L.getEditText(edt_email))) {
                edt_email.setError("Check Email ID");
            } else if (TextUtils.isEmpty(CountryID)) {
                Toast.makeText(this, "Please Select Country", Toast.LENGTH_SHORT).show();
            } else {
                if (L.isNetworkAvailable(SOPOOrderActivity.this)) {

                    AddNewSuppCust(L.getEditText(edt_cpmny), L.getEditText(edt_address), L.getEditText(edt_city), L.getEditText(edt_cont_per), L.getEditText(edt_cont_no), L.getEditText(edt_email));
                }
            }

        });


        profiledialog.show();

    }

    @OnClick({R.id.img_back, R.id.img_edit, R.id.img_add, R.id.img_add_cust, R.id.btn_next, R.id.ll_dob})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.img_edit:
                break;
            case R.id.img_add:
            case R.id.img_add_cust:
                if (spnDivision.getSelectedItemPosition() == 0 || DivisionID == null) {
                    Toast.makeText(this, "Please Select Division", Toast.LENGTH_SHORT).show();
                } else {
                    AddDialog(SOPOOrderActivity.this);
                }

                break;
            case R.id.ll_dob:
                datePickerDialog = new DatePickerDialog(SOPOOrderActivity.this, todate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                //datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
                break;
            case R.id.btn_next:
                Validation();
                break;
        }
    }

    public void Validation() {

        if (spnCmpny.getSelectedItemPosition() == 0 || CompanyID == null) {
            Toast.makeText(this, "Please Select Company", Toast.LENGTH_SHORT).show();
        } else if (spnDivision.getSelectedItemPosition() == 0 || DivisionID == null) {
            Toast.makeText(this, "Please Select Division", Toast.LENGTH_SHORT).show();
        } else if (spnSubDivision.getSelectedItemPosition() == 0 || SubDivisionID == null) {
            //  Toast.makeText(this, "Please Select SubDivision", Toast.LENGTH_SHORT).show();
            L.alert(this, "No subdivision available for this division");
        } else if (From.equalsIgnoreCase("SO") && TextUtils.isEmpty(CustomerID)) {
            Toast.makeText(this, "Please Select Customer", Toast.LENGTH_SHORT).show();
        } else if (From.equalsIgnoreCase("PO") && TextUtils.isEmpty(SupplierID)) {
            Toast.makeText(this, "Please Select Supplier", Toast.LENGTH_SHORT).show();
        } else if (spnCurrency.getSelectedItemPosition() == 0 || CurrencyID == null) {
            Toast.makeText(this, "Please Select Currency", Toast.LENGTH_SHORT).show();
        } else if (spnPTerm.getSelectedItemPosition() == 0 || PaymentTermID == null) {
            Toast.makeText(this, "Please Select PaymentTerm", Toast.LENGTH_SHORT).show();
        } else if (spnDTerm.getSelectedItemPosition() == 0 || DeliveryTermID == null) {
            Toast.makeText(this, "Please Select DeliveryTerm", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(tvDinvoice.getText().toString().trim())) {
            Toast.makeText(this, "Please Select OrderDate", Toast.LENGTH_SHORT).show();
        } else {
            OrderEntity orderEntity = new OrderEntity();

            if (Type != null && Type.equalsIgnoreCase("Edit")) {
                orderEntity.setOrderID(orderSOPODetails.getOrderID());
                orderEntity.setOrderNo(orderSOPODetails.getOrderNo());
                if (!orderSOPODetails.getDivisionID().equalsIgnoreCase(DivisionID)) {

                    sopoMaterials.clear();
                    orderSOPODetails.setTotalOrderAmount("");
                    orderSOPODetails.setRemark("");
                    if (From.equalsIgnoreCase("SO")) {
                        prefs.save(Constant.MaterialDataSO, null);
                    } else if (From.equalsIgnoreCase("PO")) {
                        prefs.save(Constant.MaterialDataPO, null);
                    }
                }

            } else {

                if (prefs != null && getData != null) {
                    if (!orderEntitys.getCompanyID().equalsIgnoreCase(CompanyID)) {

                        // sopoMaterials.clear();
                        //orderSOPODetails.setTotalOrderAmount("");
                        // orderSOPODetails.setRemark("");

                        if (From.equalsIgnoreCase("SO")) {
                            prefs.save(Constant.MaterialDataSO, null);
                        } else if (From.equalsIgnoreCase("PO")) {
                            prefs.save(Constant.MaterialDataPO, null);
                        }
                    }
                }


                orderEntity.setOrderID("0");
                orderEntity.setOrderNo("");
            }


            orderEntity.setOrderStatus("P");
            orderEntity.setCreatedBy(user.getUserID());

            orderEntity.setCompanyID(CompanyID);
            orderEntity.setDivisionID(DivisionID);
            orderEntity.setSubDivisionID(SubDivisionID);
            orderEntity.setCurrencyID(CurrencyID);
            orderEntity.setCurrencyType(CurrencyType);
            orderEntity.setPaymentTermID(PaymentTermID);
            orderEntity.setDeliveryTermID(DeliveryTermID);
            orderEntity.setOrderDate(tvDinvoice.getText().toString().trim());
            orderEntity.setLoadingType(rdDirect.isChecked() ? "DIRECT" : "WAREHOUSE");
            orderEntity.setCommissionID(COMMISSIONAGENTID);

            if (From.equalsIgnoreCase("SO")) {
                orderEntity.setPartyID(CustomerID);
                orderEntity.setOrderType(From);
                prefs.save(Constant.OrderEntitySO, (new Gson().toJson(orderEntity)));
            } else {
                orderEntity.setPartyID(SupplierID);
                orderEntity.setOrderType(From);
                prefs.save(Constant.OrderEntityPO, (new Gson().toJson(orderEntity)));
            }

            startActivity(new Intent(SOPOOrderActivity.this, SOPOMaterialListActivity.class)
                    .putExtra("FROM", From)
                    .putExtra(Constant.Type, OrderType)
                    .putExtra(Constant.OrderEntity, orderEntity)
                    .putExtra(Constant.MaterialData, mainDataModel)
                    .putExtra(Constant.sopoMaterial, (Serializable) sopoMaterials)
                    .putExtra(Constant.OrderSOPODetails, orderSOPODetails));
        }


    }


    private void getDTPTC() {
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getDTPTC(From.equalsIgnoreCase("SO") ? "SO" : "PO", user.getUserID()), (data) -> {
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.getBoolean("Status")) {

                        //Get DTPTC
                        dtptcMainModel = LoganSquare.parse(jsonResponse.toString(), DTPTCMainModel.class);

                        CompanyList.clear();
                        CompanyList.addAll(dtptcMainModel.getCompanies());
                        companyAdapter.notifyDataSetChanged();

                        paymentTermsList.clear();
                        paymentTermsList.addAll(dtptcMainModel.getPaymentTerms());
                        paymentTermsAdapter.notifyDataSetChanged();

                        deliveryTermsList.clear();
                        deliveryTermsList.addAll(dtptcMainModel.getDeliveryTerms());
                        deliveryTermsAdapter.notifyDataSetChanged();

                        currenciesList.clear();
                        currenciesList.addAll(dtptcMainModel.getCurrencies());
                        currenciesAdapter.notifyDataSetChanged();

                        //Edit Time

                        if (Type != null && Type.equalsIgnoreCase("Edit")) {
                            Log.d(TAG, "getDTPTC: " + orderSOPODetails.getLoadingType());
                            if (orderSOPODetails.getLoadingType().equalsIgnoreCase("DIRECT")) {
                                rdDirect.setChecked(true);
                            } else {
                                rdWarehouse.setChecked(true);
                            }
                            for (int i = 0; i < dtptcMainModel.getCompanies().size(); i++) {
                                if (orderSOPODetails.getCompanyID().equalsIgnoreCase(dtptcMainModel.getCompanies().get(i).getRecID())) {
                                    spnCmpny.setSelection(i + 1);
                                    break;
                                }

                            }

                            for (int i = 0; i < dtptcMainModel.getPaymentTerms().size(); i++) {
                                if (orderSOPODetails.getPaymentTermID().equalsIgnoreCase(dtptcMainModel.getPaymentTerms().get(i).getRecID())) {
                                    spnPTerm.setSelection(i + 1);
                                    break;
                                }
                            }

                            for (int i = 0; i < dtptcMainModel.getDeliveryTerms().size(); i++) {
                                if (orderSOPODetails.getDeliveryTermID().equalsIgnoreCase(dtptcMainModel.getDeliveryTerms().get(i).getRecID())) {
                                    spnDTerm.setSelection(i + 1);
                                    break;
                                }
                            }

                            for (int i = 0; i < dtptcMainModel.getCurrencies().size(); i++) {
                                if (orderSOPODetails.getCurrencyID().equalsIgnoreCase(dtptcMainModel.getCurrencies().get(i).getRecID())) {
                                    spnCurrency.setSelection(i + 1);
                                    CurrencyType = dtptcMainModel.getCurrencies().get(i - 1).getCurCode();
                                    break;
                                }
                            }


                        }


                        // Local Storage

                        if (orderEntitys != null) {
                            if (orderEntitys.getLoadingType().equalsIgnoreCase("DIRECT")) {
                                rdDirect.setChecked(true);
                            } else {
                                rdWarehouse.setChecked(true);
                            }
                            for (int i = 0; i < dtptcMainModel.getCompanies().size(); i++) {
                                if (orderEntitys.getCompanyID().equalsIgnoreCase(dtptcMainModel.getCompanies().get(i).getRecID())) {
                                    spnCmpny.setSelection(i + 1);
                                    break;
                                }
                            }

                            for (int i = 0; i < dtptcMainModel.getPaymentTerms().size(); i++) {
                                if (orderEntitys.getPaymentTermID().equalsIgnoreCase(dtptcMainModel.getPaymentTerms().get(i).getRecID())) {
                                    spnPTerm.setSelection(i + 1);
                                    break;
                                }
                            }

                            for (int i = 0; i < dtptcMainModel.getDeliveryTerms().size(); i++) {
                                if (orderEntitys.getDeliveryTermID().equalsIgnoreCase(dtptcMainModel.getDeliveryTerms().get(i).getRecID())) {
                                    spnDTerm.setSelection(i + 1);
                                    break;
                                }
                            }
                            for (int i = 0; i < dtptcMainModel.getCurrencies().size(); i++) {
                                if (orderEntitys.getCurrencyID().equalsIgnoreCase(dtptcMainModel.getCurrencies().get(i).getRecID())) {
                                    spnCurrency.setSelection(i + 1);
                                    break;
                                }

                            }
                        }


                    } else {
                        L.alert(SOPOOrderActivity.this, jsonResponse.getString("UMessage"));

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                showProgress(false);

            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    private void getDivision() {
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getDivision(From.equalsIgnoreCase("SO") ? "SO" : "PO", user.getUserID(), CompanyIDGet), (data) -> {
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.getBoolean("Status")) {

                        //Get Division
                        divisionModel = LoganSquare.parse(jsonResponse.toString(), DivisionModel.class);

                        //Division List
                        divisionAdapter = new DivisionAdapter(this, divisionModel.getDivisions());
                        spnDivision.setAdapter(divisionAdapter);

                        if (Type != null && Type.equalsIgnoreCase("Edit")) {
                            tvDinvoice.setText(orderSOPODetails.getOrderDate());
                            for (int i = 0; i < divisionModel.getDivisions().size(); i++) {
                                if (orderSOPODetails.getDivisionID().equalsIgnoreCase(divisionModel.getDivisions().get(i).getRecID())) {
                                    spnDivision.setSelection(i + 1);
                                }
                            }
                        }

                        if (orderEntitys != null) {
                            tvDinvoice.setText(orderEntitys.getOrderDate());
                            for (int i = 0; i < divisionModel.getDivisions().size(); i++) {
                                if (orderEntitys.getDivisionID().equalsIgnoreCase(divisionModel.getDivisions().get(i).getRecID())) {
                                    spnDivision.setSelection(i + 1);
                                }
                            }
                        }

                        if (divisionModel.getDivisions().size() != 0) {
                            liDivision.setVisibility(View.VISIBLE);
                            viDivision.setVisibility(View.VISIBLE);
                        } else {
                            liDivision.setVisibility(View.GONE);
                            viDivision.setVisibility(View.GONE);

                        }


                    } else {
                        L.alert(SOPOOrderActivity.this, jsonResponse.getString("UMessage"));

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                showProgress(false);

            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    private void getSubDivision() {
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getSubDivisionwithOther(From.equalsIgnoreCase("SO") ? "SO" : "PO", user.getUserID(), DivisionID), (data) -> {
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.getBoolean("Status")) {

                        //Get SubDivision
                        mainDataModel = LoganSquare.parse(jsonResponse.toString(), MainDataModel.class);

                        //SubDivision List
                        if (mainDataModel.getSubDivisions() != null) {
                            subdivisionAdapter = new SubDivisionAdapter(this, mainDataModel.getSubDivisions());
                            spnSubDivision.setAdapter(subdivisionAdapter);
                        }

                        if (Type != null && Type.equalsIgnoreCase("Edit")) {
                            for (int i = 0; i < mainDataModel.getSubDivisions().size(); i++) {
                                if (orderSOPODetails.getSubDivisionID().equalsIgnoreCase(mainDataModel.getSubDivisions().get(i).getRecID())) {
                                    spnSubDivision.setSelection(i + 1);
                                }
                            }
                        }

                        if (orderEntitys != null && mainDataModel.getSubDivisions() != null) {
                            for (int i = 0; i < mainDataModel.getSubDivisions().size(); i++) {
                                if (orderEntitys.getSubDivisionID().equalsIgnoreCase(mainDataModel.getSubDivisions().get(i).getRecID())) {
                                    spnSubDivision.setSelection(i + 1);
                                }
                            }
                        }

                        if (From.equalsIgnoreCase("SO")) {
                            liSupplier.setVisibility(View.GONE);
                            viewSupplier.setVisibility(View.GONE);

                            viewCustomer.setVisibility(View.VISIBLE);
                            liCustomer.setVisibility(View.VISIBLE);

                        } else {
                            liSupplier.setVisibility(View.VISIBLE);
                            viewSupplier.setVisibility(View.VISIBLE);

                            viewCustomer.setVisibility(View.GONE);
                            liCustomer.setVisibility(View.GONE);
                        }

                        if (mainDataModel.getSubDivisions() != null && mainDataModel.getSubDivisions().size() != 0) {
                            liSubDivision.setVisibility(View.VISIBLE);
                            viSubdivision.setVisibility(View.VISIBLE);
                        } else {
                            L.alert(this, "No Subdivision Available For This Division");
                            liSubDivision.setVisibility(View.GONE);
                            viSubdivision.setVisibility(View.GONE);
                        }

                        if (From.equalsIgnoreCase("PO")) {
                            //Supplier List
                            strSupplierArray.clear();
                            for (int i = 0; i < mainDataModel.getSuppliers().size(); i++) {
                                strSupplierArray.add(mainDataModel.getSuppliers().get(i).getCompanyName());
                            }
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>
                                    (this, android.R.layout.select_dialog_item, strSupplierArray);

                            Ac_supplier.setThreshold(0);//will start working from first character
                            Ac_supplier.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                            Ac_supplier.setTextColor(Color.BLACK);
                            //***************************************************************
                            if (Type != null && Type.equalsIgnoreCase("Edit")) {
                                for (int i = 0; i < mainDataModel.getSuppliers().size(); i++) {
                                    if (orderSOPODetails.getPartyID().equalsIgnoreCase(mainDataModel.getSuppliers().get(i).getRecID())) {
                                        Ac_supplier.setText(mainDataModel.getSuppliers().get(i).getCompanyName());
                                        SupplierID = mainDataModel.getSuppliers().get(i).getRecID();
                                    }
                                }
                            }

                            if (orderEntitys != null) {
                                for (int i = 0; i < mainDataModel.getSuppliers().size(); i++) {
                                    if (orderEntitys.getPartyID().equalsIgnoreCase(mainDataModel.getSuppliers().get(i).getRecID())) {
                                        Ac_supplier.setText(mainDataModel.getSuppliers().get(i).getCompanyName());
                                        SupplierID = mainDataModel.getSuppliers().get(i).getRecID();

                                    }
                                }
                            }

                        } else {
                            //Customer List
                            strCustomerArray.clear();
                            for (int i = 0; i < mainDataModel.getCustomers().size(); i++) {
                                strCustomerArray.add(mainDataModel.getCustomers().get(i).getCompanyName());
                            }
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>
                                    (this, android.R.layout.select_dialog_item, strCustomerArray);

                            AC_compny.setThreshold(0);//will start working from first character
                            AC_compny.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                            AC_compny.setTextColor(Color.BLACK);
                            //***************************************************************

                            if (Type != null && Type.equalsIgnoreCase("Edit")) {
                                for (int i = 0; i < mainDataModel.getCustomers().size(); i++) {
                                    if (orderSOPODetails.getPartyID().equalsIgnoreCase(mainDataModel.getCustomers().get(i).getRecID())) {
                                        AC_compny.setText(mainDataModel.getCustomers().get(i).getCompanyName());
                                        CustomerID = mainDataModel.getCustomers().get(i).getRecID();
                                        //  AC_compny.setSelection(i);
                                        // AC_compny.setThreshold(1);
                                    }
                                }
                            }

                            if (orderEntitys != null) {
                                for (int i = 0; i < mainDataModel.getCustomers().size(); i++) {
                                    if (orderEntitys.getPartyID().equalsIgnoreCase(mainDataModel.getCustomers().get(i).getRecID())) {
                                        AC_compny.setText(mainDataModel.getCustomers().get(i).getCompanyName());
                                        CustomerID = mainDataModel.getCustomers().get(i).getRecID();

                                        //  AC_compny.setSelection(i);
                                        // AC_compny.setThreshold(1);
                                    }
                                }
                            }
                        }


                    } else {
                        L.alert(SOPOOrderActivity.this, jsonResponse.getString("UMessage"));

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                showProgress(false);

            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    private void getAllCommissionAgent() {
        /*"BELPSTADM000034"*/
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getAllCommissionAgent(From.equalsIgnoreCase("SO") ? "S" : "P", new SimpleDateFormat("yyyy-MM-dd").format(new Date()), SubDivisionID), (data) -> {
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.getBoolean("Status")) {
                        Log.d(TAG, "getAllCommissionAgent: " + jsonResponse.toString());

                        //Get AllCommission Agent List
                        commissionagentmodel = LoganSquare.parse(jsonResponse.toString(), CommissionAgentModel.class);

                        //AllCommission Agent List
                        if (commissionagentmodel.getCommissions() != null) {
                            commissionAgentAdapter = new CommissionAgentAdapter(this, commissionagentmodel.getCommissions());
                            spnCommissionAgent.setAdapter(commissionAgentAdapter);
                        }

                        if (commissionagentmodel.getCommissions() != null && commissionagentmodel.getCommissions().size() != 0) {
                            liCommissionAgent.setVisibility(View.VISIBLE);
                            viCommision.setVisibility(View.VISIBLE);
                        } else {
                            L.alert(this, "No Commission Agent Available For This Sub Division");
                            liCommissionAgent.setVisibility(View.GONE);
                            viCommision.setVisibility(View.GONE);
                        }

                        if (Type != null && Type.equalsIgnoreCase("Edit")) {
                            for (int i = 0; i < commissionagentmodel.getCommissions().size(); i++) {
                                if (orderSOPODetails.getCommissionID().equalsIgnoreCase(commissionagentmodel.getCommissions().get(i).getRecID())) {
                                    spnCommissionAgent.setSelection(i + 1);
                                }
                            }
                        }

                        if (orderEntitys != null && commissionagentmodel.getCommissions() != null) {
                            for (int i = 0; i < commissionagentmodel.getCommissions().size(); i++) {
                                if (orderEntitys.getCommissionID().equalsIgnoreCase(commissionagentmodel.getCommissions().get(i).getRecID())) {
                                    spnCommissionAgent.setSelection(i + 1);
                                }
                            }
                        }

                    } else {
                        L.alert(SOPOOrderActivity.this, jsonResponse.getString("UMessage"));

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                showProgress(false);

            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    private void getCountry() {
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getCountryList(), (data) -> {
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.getBoolean("Status")) {

                        //Get AllCountry
                        allCountry = LoganSquare.parseList(jsonResponse.getJSONArray("Country").toString(), AllCountry.class);
                        //Country List
                        strCountryArray.clear();
                        for (int i = 0; i < allCountry.size(); i++) {
                            strCountryArray.add(allCountry.get(i).getCountryName());
                        }


                    } else {
                        L.alert(SOPOOrderActivity.this, jsonResponse.getString("UMessage"));

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                showProgress(false);

            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    private void AddNewSuppCust(String compnayname, String address, String city, String
            contactperson, String contactno, String email) {

        Map<String, String> map = new HashMap<>();
        map.put("CompanyType", CompanyType);
        map.put("CompanyName", compnayname);
        map.put("Address", address);
        map.put("CountryID", CompanyID);
        map.put("City", city);
        map.put("ContactPerson", contactperson);
        map.put("ContactNo", contactno);
        map.put("Email", email);
        map.put("DivisionID", DivisionID);
        map.put("UserID", user.getUserID());
        map.put("OrderType", From);

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.AddNewSuppCust(map), (data) -> {
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.getBoolean("Status")) {

                        mainDataModel = LoganSquare.parse(jsonResponse.toString(), MainDataModel.class);

                        profiledialog.dismiss();

                        if (From.equalsIgnoreCase("PO")) {
                            //Supplier List
                            strSupplierArray.clear();
                            for (int i = 0; i < mainDataModel.getSuppliers().size(); i++) {
                                strSupplierArray.add(mainDataModel.getSuppliers().get(i).getCompanyName());
                            }
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>
                                    (this, android.R.layout.select_dialog_item, strSupplierArray);

                            Ac_supplier.setThreshold(0);//will start working from first character
                            Ac_supplier.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                            Ac_supplier.setTextColor(Color.BLACK);
                            //***************************************************************
                        } else {
                            //Customer List
                            strCustomerArray.clear();
                            for (int i = 0; i < mainDataModel.getCustomers().size(); i++) {
                                strCustomerArray.add(mainDataModel.getCustomers().get(i).getCompanyName());
                            }
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>
                                    (this, android.R.layout.select_dialog_item, strCustomerArray);

                            AC_compny.setThreshold(0);//will start working from first character
                            AC_compny.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                            AC_compny.setTextColor(Color.BLACK);
                            //***************************************************************
                        }

                    } else {
                        L.alert(SOPOOrderActivity.this, jsonResponse.getString("UMessage"));

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                showProgress(false);

            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }


}

class CompanyAdapter extends BaseAdapter {
    List<DTPTCItemModel> compnylist;
    LayoutInflater inflter;
    Context context;

    public CompanyAdapter(Context context, List<DTPTCItemModel> compnylists) {
        this.context = context;
        this.compnylist = compnylists;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return compnylist.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return compnylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.custom_spinner_items, null);
        TextView names = convertView.findViewById(R.id.tv_spinner);
        if (position == 0) {
            names.setText("Select Company");
            names.setTextColor(Color.GRAY);
        } else {
            DTPTCItemModel dtptcCMPYModel = compnylist.get(position - 1);
            names.setText(dtptcCMPYModel.getCompanyName());
            names.setTextColor(Color.BLACK);
        }
        return convertView;
    }


}

class PaymentTermsAdapter extends BaseAdapter {
    List<DTPTCItemModel> PTItemList;
    LayoutInflater inflter;
    Context context;

    public PaymentTermsAdapter(Context context, List<DTPTCItemModel> PTItemLists) {
        this.context = context;
        this.PTItemList = PTItemLists;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return PTItemList.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return PTItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.custom_spinner_items, null);
        TextView names = convertView.findViewById(R.id.tv_spinner);
        if (position == 0) {
            names.setText("Select Payment Term");
            names.setTextColor(Color.GRAY);
        } else {
            DTPTCItemModel dtptcCMPYModel = PTItemList.get(position - 1);
            names.setText(dtptcCMPYModel.getPTDesc());
            names.setTextColor(Color.BLACK);
        }
        return convertView;
    }


}

class DeliveryTermsAdapter extends BaseAdapter {
    List<DTPTCItemModel> DTItemList;
    LayoutInflater inflter;
    Context context;

    public DeliveryTermsAdapter(Context context, List<DTPTCItemModel> DTItemLists) {
        this.context = context;
        this.DTItemList = DTItemLists;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return DTItemList.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return DTItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.custom_spinner_items, null);
        TextView names = convertView.findViewById(R.id.tv_spinner);
        if (position == 0) {
            names.setText("Select DeliveryTerms");
            names.setTextColor(Color.GRAY);
        } else {
            DTPTCItemModel dtptcCMPYModel = DTItemList.get(position - 1);
            names.setText(dtptcCMPYModel.getPTDesc());
            names.setTextColor(Color.BLACK);
        }
        return convertView;
    }


}

class CurrenciesAdapter extends BaseAdapter {
    List<DTPTCItemModel> currenciesList;
    LayoutInflater inflter;
    Context context;

    public CurrenciesAdapter(Context context, List<DTPTCItemModel> currenciesLists) {
        this.context = context;
        this.currenciesList = currenciesLists;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return currenciesList.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return currenciesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.custom_spinner_items, null);
        TextView names = convertView.findViewById(R.id.tv_spinner);
        if (position == 0) {
            names.setText("Select Currency");
            names.setTextColor(Color.GRAY);
        } else {
            DTPTCItemModel dtptcCMPYModel = currenciesList.get(position - 1);
            names.setText(dtptcCMPYModel.getCurCode());
            names.setTextColor(Color.BLACK);
        }
        return convertView;
    }


}

class DivisionAdapter extends BaseAdapter {
    List<DTPTCItemModel> divisionlist;
    LayoutInflater inflter;
    Context context;

    public DivisionAdapter(Context context, List<DTPTCItemModel> divisionlists) {
        this.context = context;
        this.divisionlist = divisionlists;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return divisionlist.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return divisionlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.custom_spinner_items, null);
        TextView names = convertView.findViewById(R.id.tv_spinner);
        if (position == 0) {
            names.setText("Select Division");
            names.setTextColor(Color.GRAY);
        } else {
            DTPTCItemModel dtptcCMPYModel = divisionlist.get(position - 1);
            names.setText(dtptcCMPYModel.getDivisionName());
            names.setTextColor(Color.BLACK);
        }
        return convertView;
    }


}

class SubDivisionAdapter extends BaseAdapter {
    List<DTPTCItemModel> subdivisionlist;
    LayoutInflater inflter;
    Context context;

    public SubDivisionAdapter(Context context, List<DTPTCItemModel> subdivisionlists) {
        this.context = context;
        this.subdivisionlist = subdivisionlists;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return subdivisionlist.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return subdivisionlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.custom_spinner_items, null);
        TextView names = convertView.findViewById(R.id.tv_spinner);
        if (position == 0) {
            names.setText("Select SubDivision");
            names.setTextColor(Color.GRAY);
        } else {
            DTPTCItemModel dtptcCMPYModel = subdivisionlist.get(position - 1);
            names.setText(dtptcCMPYModel.getSubDivisionName());
            names.setTextColor(Color.BLACK);
        }
        return convertView;
    }


}

class CommissionAgentAdapter extends BaseAdapter {
    List<DTPTCItemModel> CommissionAgentList;
    LayoutInflater inflter;
    Context context;

    public CommissionAgentAdapter(Context context, List<DTPTCItemModel> CommissionAgentLists) {
        this.context = context;
        this.CommissionAgentList = CommissionAgentLists;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return CommissionAgentList.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return CommissionAgentList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.custom_spinner_items, null);
        TextView names = convertView.findViewById(R.id.tv_spinner);
        if (position == 0) {
            names.setText("Select Commission Agent");
            names.setTextColor(Color.GRAY);
        } else {
            DTPTCItemModel dtptcCMPYModel = CommissionAgentList.get(position - 1);
            names.setText(dtptcCMPYModel.getCommGroupName());
            names.setTextColor(Color.BLACK);
        }
        return convertView;
    }


}

