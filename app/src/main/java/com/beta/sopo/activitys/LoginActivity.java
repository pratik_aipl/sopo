package com.beta.sopo.activitys;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beta.sopo.BaseActivity;
import com.beta.sopo.BuildConfig;
import com.beta.sopo.R;
import com.beta.sopo.model.UserData;
import com.beta.sopo.network.NetworkRequest;
import com.beta.sopo.utils.Constant;
import com.beta.sopo.utils.L;
import com.beta.sopo.utils.Validation;
import com.bluelinelabs.logansquare.LoganSquare;
import com.google.gson.Gson;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Credentials;
import rx.Subscription;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.edt_email)
    EditText edtEmail;
    @BindView(R.id.edt_paswd)
    EditText edtPaswd;
    @BindView(R.id.tv_forgot)
    TextView tvForgot;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.tv_register)
    TextView tvRegister;
    @BindView(R.id.lin_signup)
    LinearLayout linSignup;

    Subscription subscription;
    @BindView(R.id.img_login_pswd)
    ImageView imgLoginPswd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        tvRegister.setPaintFlags(tvRegister.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        if (BuildConfig.DEBUG) {
            edtEmail.setText("dv@additinfotech.com");
            edtPaswd.setText("12345678");
            //edtEmail.setText("abhishek.devadiga@premierinfo.biz");
           // edtPaswd.setText("abhishek");
        }
        if (prefs != null) {
            edtEmail.setText(prefs.getString(Constant.USERNAME, ""));
        }

        imgLoginPswd.setOnTouchListener((v, event) -> {

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    edtPaswd.setInputType(InputType.TYPE_CLASS_TEXT);
                    break;
                case MotionEvent.ACTION_UP:
                    edtPaswd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    break;
            }
            return true;
        });
    }


    @OnClick({R.id.tv_forgot, R.id.btn_login, R.id.tv_register, R.id.lin_signup})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_forgot:
                startActivity(new Intent(LoginActivity.this, ForgotPassword.class));
                break;
            case R.id.btn_login:
                validation();
                break;
            case R.id.tv_register:
                //  startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                startActivity(new Intent(this, RegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
            case R.id.lin_signup:
                break;
        }
    }

    public void validation() {
        if (!Validation.isValidEmail(L.getEditText(edtEmail))) {
            edtEmail.setError("Check Email ID");
        } else if (Validation.isEmpty(L.getEditText(edtPaswd))) {
            edtPaswd.setError("Please enter Password");
        } else {
            if (L.isNetworkAvailable(LoginActivity.this)) {
                Login();
            }
        }
    }

    @Override
    public void onBackPressed() {
        exitalert();
    }

    public void exitalert() {
        new AlertDialog.Builder(this)
                .setMessage(("Do you want to exit?"))
                .setPositiveButton("YES", (dialog, which) -> {
                    finish();
                })
                .setNegativeButton("NO", null)
                .show();
    }

    private void Login() {
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getLogin(Credentials.basic(L.getEditText(edtEmail), L.getEditText(edtPaswd))), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.getBoolean("Status")) {


                        UserData user = LoganSquare.parse(jsonResponse.toString(), UserData.class);

                        prefs.save(Constant.loginAuthToken, user.getTokenEntity().getAuthToken());

                        prefs.save(Constant.UserData, new Gson().toJson(user));
                        prefs.save(Constant.isLogin, true);

                        prefs.save(Constant.USERNAME, L.getEditText(edtEmail));

                        //Toast.makeText(this, jsonResponse.getString("UMessage"), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(this, DashboardActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                                Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                    } else {
                        L.alert(LoginActivity.this, jsonResponse.getString("UMessage"));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                if (data.code() == 401) {
                    L.alert(LoginActivity.this, "Email Id is Wrong...!");
                }
                //L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }
}
