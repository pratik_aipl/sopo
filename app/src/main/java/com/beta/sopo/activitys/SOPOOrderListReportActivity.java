package com.beta.sopo.activitys;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.beta.sopo.BaseActivity;
import com.beta.sopo.R;
import com.beta.sopo.adpater.OrderListAdpater;
import com.beta.sopo.model.OrderSOPO;
import com.beta.sopo.network.NetworkRequest;
import com.beta.sopo.utils.Constant;
import com.beta.sopo.utils.L;
import com.bluelinelabs.logansquare.LoganSquare;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class SOPOOrderListReportActivity extends BaseActivity {

    OrderListAdpater orderListAdpater;
    String From, Back;

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.img_edit)
    ImageView imgEdit;
    @BindView(R.id.img_addd)
    ImageView imgAddd;
    @BindView(R.id.lli)
    LinearLayout lli;
    @BindView(R.id.lin_header)
    LinearLayout linHeader;
    @BindView(R.id.tv_tittle2)
    TextView tvTittle2;
    @BindView(R.id.list)
    LinearLayout list;
    @BindView(R.id.recycler_list)
    RecyclerView OrderList;

    Subscription subscription;
    private static final String TAG = "SOPOOrderListActivity";

    List<OrderSOPO> Ordersopo = new ArrayList<>();
    @BindView(R.id.tv_fromDate)
    TextView tvFromDate;
    @BindView(R.id.tv_toDate)
    TextView tvToDate;
    @BindView(R.id.btn_search)
    Button btnSearch;
    @BindView(R.id.lin_reports)
    LinearLayout linReports;
    @BindView(R.id.tv_nodata)
    TextView tvNodata;

    DatePickerDialog datePickerDialog;
    final Calendar myCalendar = Calendar.getInstance();
    String ToDATE = "", FromDATE = "";
    Date datda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.activity_sopoorder_reports);
        ButterKnife.bind(this);


        imgAddd.setVisibility(View.GONE);
        imgAddd.setImageResource(R.drawable.filter);

        if (getIntent() != null)
            From = getIntent().getStringExtra("FROM");
        Back = getIntent().getStringExtra("Back");
        if (From.equalsIgnoreCase("SO")) {
            tvTittle.setText("SO REPORT");
            tvTittle2.setText("SO REPORT LIST");

        } else {
            tvTittle.setText("PO REPORT");
            tvTittle2.setText("PO REPORT LIST");
        }

        OrderList.setHasFixedSize(true);
        OrderList.setItemAnimator(new DefaultItemAnimator());
        OrderList.setLayoutManager(new LinearLayoutManager(this));
        orderListAdpater = new OrderListAdpater(this, Ordersopo, From);
        OrderList.setAdapter(orderListAdpater);

    }

    @OnClick({R.id.img_back, R.id.btn_search, R.id.tv_toDate, R.id.tv_fromDate})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                if (Back != null && Back.equalsIgnoreCase("Dash")) {
                    startActivity(new Intent(SOPOOrderListReportActivity.this, DashboardActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                    Intent.FLAG_ACTIVITY_SINGLE_TOP |
                                    Intent.FLAG_ACTIVITY_CLEAR_TOP));

                } else {
                    onBackPressed();
                }
                break;
            case R.id.btn_search:
                if (L.isNetworkAvailable(SOPOOrderListReportActivity.this)) {
                    if (TextUtils.isEmpty(FromDATE)) {
                        Toast.makeText(this, "Please Select From Date", Toast.LENGTH_SHORT).show();
                    } else if (TextUtils.isEmpty(ToDATE)) {
                        Toast.makeText(this, "Please Select To Date", Toast.LENGTH_SHORT).show();
                    } else {
                        getSOPOList();
                    }
                }

                break;
            case R.id.tv_toDate:
                datePickerDialog = new DatePickerDialog(SOPOOrderListReportActivity.this, todate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.getDatePicker().setMinDate(datda.getTime());
                datePickerDialog.show();

                break;
            case R.id.tv_fromDate:
                datePickerDialog = new DatePickerDialog(SOPOOrderListReportActivity.this, fromdate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();

                break;
        }
    }

    private void toDate() {
        String myFormat = "dd-MMM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        tvToDate.setText(sdf.format(myCalendar.getTime()));
        ToDATE = sdf.format(myCalendar.getTime());

    }

    private void fromDate() {
        String myFormat = "dd-MMM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        tvFromDate.setText(sdf.format(myCalendar.getTime()));
        FromDATE = sdf.format(myCalendar.getTime());
        datda = myCalendar.getTime();

    }

    DatePickerDialog.OnDateSetListener fromdate = (view, year, monthOfYear, dayOfMonth) -> {
        // TODO Auto-generated method stub
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, monthOfYear);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        fromDate();
    };
    DatePickerDialog.OnDateSetListener todate = (view, year, monthOfYear, dayOfMonth) -> {
        // TODO Auto-generated method stub
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, monthOfYear);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        toDate();
    };

    private void getSOPOList() {
        Map<String, String> map = new HashMap<>();
        map.put("OrderID", "0");
        map.put("UserID", user.getUserID());
        map.put("OrderType", From);
        map.put("FromDate", FromDATE);
        map.put("ToDate", ToDATE);

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getOrderSOPO(From.equalsIgnoreCase("SO") ? "AllSO" : "AllPO", map), (data) -> {
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.getBoolean("Status")) {
                        // Toast.makeText(this, jsonResponse.getString("UMessage"), Toast.LENGTH_SHORT).show();
                        if ((jsonResponse.getString("sopoList").equalsIgnoreCase("null"))) {
                            //OrderList.setVisibility(View.GONE);
                            tvNodata.setVisibility(View.VISIBLE);
                            Ordersopo.clear();
                            orderListAdpater.notifyDataSetChanged();
                            L.alert(SOPOOrderListReportActivity.this, jsonResponse.getString("UMessage"));
                        } else {
                            tvNodata.setVisibility(View.GONE);
                            OrderList.setVisibility(View.VISIBLE);
                            Ordersopo.clear();
                            Ordersopo.addAll(LoganSquare.parseList(jsonResponse.getJSONArray(Constant.sopoList).toString(), OrderSOPO.class));
                            orderListAdpater.notifyDataSetChanged();

                        }

                    } else {
                        L.alert(SOPOOrderListReportActivity.this, jsonResponse.getString("UMessage"));
                    }

                 /*   if (Ordersopo.size() != 0) {
                        tvNodata.setVisibility(View.GONE);
                    } else {
                        tvNodata.setVisibility(View.VISIBLE);
                       // L.alert(SOPOOrderListReportActivity.this, jsonResponse.getString("UMessage"));
                    }*/


                } catch (Exception e) {
                    e.printStackTrace();
                }
                showProgress(false);
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }
}
