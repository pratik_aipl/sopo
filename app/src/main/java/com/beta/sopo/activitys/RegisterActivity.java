package com.beta.sopo.activitys;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beta.sopo.BaseActivity;
import com.beta.sopo.R;
import com.beta.sopo.network.NetworkRequest;
import com.beta.sopo.utils.Constant;
import com.beta.sopo.utils.L;
import com.beta.sopo.utils.Validation;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class RegisterActivity extends BaseActivity {

    @BindView(R.id.edt_name)
    EditText edtName;
    @BindView(R.id.edt_email)
    EditText edtEmail;
    @BindView(R.id.edt_paswd)
    EditText edtPaswd;
    @BindView(R.id.edt_cnf_paswd)
    EditText edtCnfPaswd;
    @BindView(R.id.btn_register)
    Button btnRegister;
    @BindView(R.id.tv_login)
    TextView tvLogin;
    @BindView(R.id.lin_signup)
    LinearLayout linSignup;

    Subscription subscription;
    @BindView(R.id.img_pswd_show)
    ImageView imgPswdShow;
    @BindView(R.id.img_cnf_show)
    ImageView imgCnfShow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        tvLogin.setPaintFlags(tvLogin.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        imgCnfShow.setOnTouchListener((v, event) -> {

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    edtCnfPaswd.setInputType(InputType.TYPE_CLASS_TEXT);
                    break;
                case MotionEvent.ACTION_UP:
                    edtCnfPaswd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    break;
            }
            return true;
        });

        imgPswdShow.setOnTouchListener((v, event) -> {

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    edtPaswd.setInputType(InputType.TYPE_CLASS_TEXT);
                    break;
                case MotionEvent.ACTION_UP:
                    edtPaswd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    break;
            }
            return true;
        });

        edtPaswd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if ((L.getEditText(edtPaswd).length() < 8)) {
                    edtPaswd.setError("Please enter Minimum 8 Character");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtCnfPaswd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if ((L.getEditText(edtCnfPaswd).length() < 8)) {
                    edtCnfPaswd.setError("Please enter Minimum 8 Character");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onBackPressed() {

        startActivity(new Intent(this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TOP));
    }

    @OnClick({R.id.btn_register, R.id.tv_login, R.id.img_cnf_show, R.id.img_pswd_show})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_register:
                validation();
                break;
            case R.id.tv_login:
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                break;
            case R.id.img_cnf_show:

                break;
            case R.id.img_pswd_show:
                break;
        }
    }

    public void validation() {
        if (Validation.isEmpty(L.getEditText(edtName))) {
            edtName.setError("Please enter Name");
        } else if ((L.getEditText(edtName).length() < 3)) {
            edtName.setError("Please enter Minimum 3 Character");
        } else if (!Validation.isValidEmail(L.getEditText(edtEmail))) {
            edtEmail.setError("Check Email ID");
        } else if (Validation.isEmpty(L.getEditText(edtPaswd))) {
            edtPaswd.setError("Please enter Password");
        } else if ((L.getEditText(edtPaswd).length() < 8)) {
            edtPaswd.setError("Please enter Minimum 8 Character");
        } else if (Validation.isEmpty(L.getEditText(edtCnfPaswd))) {
            edtCnfPaswd.setError("Please enter Confirm Password");
        } else if ((L.getEditText(edtCnfPaswd).length() < 8)) {
            edtCnfPaswd.setError("Please enter Minimum 8 Character");
        } else if (!L.getEditText(edtPaswd).matches(L.getEditText(edtCnfPaswd))) {
            Toast.makeText(this, "Please Enter Same Password", Toast.LENGTH_SHORT).show();
        } else {
            if (L.isNetworkAvailable(RegisterActivity.this)) {
                Register();
            }
        }
    }

    private void Register() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.USERName, L.getEditText(edtName));
        map.put(Constant.EMAIL, L.getEditText(edtEmail));
        map.put(Constant.Password, L.getEditText(edtPaswd));
        map.put(Constant.CNFPassword, L.getEditText(edtCnfPaswd));
        map.put(Constant.DeviceID, L.getDeviceId(this));
        //  map.put(Constant.PlayerID, "123");


        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getRegister(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.getBoolean("Status")) {


                        new AlertDialog.Builder(this)
                                .setMessage(jsonResponse.getString("UMessage"))
                                .setPositiveButton("OK", (dialog, which) -> {

                                    Intent intent = new Intent(this, LoginActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                                            Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                })
                                .show();


                    } else {
                        L.alert(RegisterActivity.this, jsonResponse.getString("UMessage"));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }
}
