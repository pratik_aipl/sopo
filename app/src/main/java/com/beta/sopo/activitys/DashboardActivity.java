package com.beta.sopo.activitys;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.drawerlayout.widget.DrawerLayout;

import com.beta.sopo.BaseActivity;
import com.beta.sopo.R;
import com.beta.sopo.utils.L;
import com.google.android.material.navigation.NavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DashboardActivity extends BaseActivity {

    ActionBarDrawerToggle t;
    TextView navUsername, navmobile;
    boolean firstclick = false;
    @BindView(R.id.img_drawerOpen)
    ImageView drawerOpen;
    @BindView(R.id.tv_dashboard)
    TextView tvDashboard;
    @BindView(R.id.rel)
    RelativeLayout rel;
    @BindView(R.id.lli)
    LinearLayout lli;
    @BindView(R.id.img_so)
    ImageView imgSo;
    @BindView(R.id.lin_SO)
    LinearLayout linSO;
    @BindView(R.id.img_po)
    ImageView imgPo;
    @BindView(R.id.lin_PO)
    LinearLayout linPO;
    @BindView(R.id.nv)
    NavigationView navigationView;
    @BindView(R.id.drawer)
    DrawerLayout drawerLayout;

    ImageView drawerclose;
    Menu nav_Menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);

        nav_Menu = navigationView.getMenu();


        if (user.getUserEntity().getIsPOAllowed().equalsIgnoreCase("0")) {
            nav_Menu.findItem(R.id.addPOorder).setVisible(false);
            nav_Menu.findItem(R.id.POReport).setVisible(false);
            linPO.setVisibility(View.GONE);
        }
        if (user.getUserEntity().getIsSOAllowed().equalsIgnoreCase("0")) {
            linSO.setVisibility(View.GONE);
            nav_Menu.findItem(R.id.addSOorder).setVisible(false);
            nav_Menu.findItem(R.id.SOreport).setVisible(false);
        }

        drawerLayout.addDrawerListener(t);
        navigationView.setItemIconTintList(null);

        View headerView = navigationView.getHeaderView(0);
        navUsername = headerView.findViewById(R.id.tv_username);
        navmobile = headerView.findViewById(R.id.tv_email_id);
        drawerclose = headerView.findViewById(R.id.img_drawerclose);

        navUsername.setText(user.getUserEntity().getUserName());
        navmobile.setText(user.getUserEntity().getUserEmail());

        drawerclose.setOnClickListener(v -> drawerLayout.closeDrawer(Gravity.LEFT));


        navigationView.setNavigationItemSelectedListener(item -> {
            int id = item.getItemId();
            switch (id) {
                case R.id.addSOorder:
                    selectItem(0);
                    break;
                case R.id.addPOorder:
                    selectItem(1);
                    break;
                case R.id.ChangePaswd:
                    selectItem(2);
                    break;
                case R.id.logout:
                    selectItem(3);
                    break;
                case R.id.SOreport:
                    selectItem(4);
                    break;
                case R.id.POReport:
                    selectItem(5);
                    break;
                default:
                    break;
            }
            return true;

        });


    }

    private void selectItem(int position) {

        switch (position) {
            case 0:
                drawerLayout.closeDrawer(Gravity.LEFT);
                //Intent myIntent = new Intent(DashboardActivity.this, SOPOOrderListActivity.class);
                Intent myIntent = new Intent(DashboardActivity.this, SOPOOrderActivity.class);
                myIntent.putExtra("FROM", "SO");
                startActivity(myIntent);
                break;
            case 1:
                drawerLayout.closeDrawer(Gravity.LEFT);
                Intent in = new Intent(DashboardActivity.this, SOPOOrderActivity.class);
                in.putExtra("FROM", "PO");
                startActivity(in);
                break;
            case 2:
                startActivity(new Intent(DashboardActivity.this, ChangePassword.class));
                break;
            case 3:
                alert();
                break;
            case 4:
                drawerLayout.closeDrawer(Gravity.LEFT);
                //Intent myIntent = new Intent(DashboardActivity.this, SOPOOrderListActivity.class);
                Intent inte = new Intent(DashboardActivity.this, SOPOOrderListReportActivity.class);
                inte.putExtra("FROM", "SO");
                startActivity(inte);
                break;
            case 5:
                drawerLayout.closeDrawer(Gravity.LEFT);
                Intent inn = new Intent(DashboardActivity.this, SOPOOrderListReportActivity.class);
                inn.putExtra("FROM", "PO");
                startActivity(inn);
                break;
            default:
                break;
        }

    }

    @Override
    public void onBackPressed() {
        if (!firstclick)
            exitalert();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (t.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }

    public void exitalert() {
        new AlertDialog.Builder(this)
                .setMessage(("Are you sure to Exit?"))
                .setPositiveButton("YES", (dialog, which) -> {

                    firstclick = false;
                    finish();

                })
                .setNegativeButton("NO", null)
                .show();
    }

    public void alert() {
        new AlertDialog.Builder(this)
                .setMessage((R.string.logout))
                .setPositiveButton("YES", (dialog, which) -> {
                    //getLogout();
                    L.logout(DashboardActivity.this);

                })
                .setNegativeButton("NO", null)
                .show();
    }

    @OnClick({R.id.img_drawerOpen, R.id.lin_SO, R.id.lin_PO})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_drawerOpen:
                drawerLayout.openDrawer(Gravity.LEFT);
                break;
            case R.id.lin_SO:
                Intent myIntent = new Intent(DashboardActivity.this, SOPOOrderListActivity.class);
                myIntent.putExtra("FROM", "SO");
                startActivity(myIntent);
                break;
            case R.id.lin_PO:
                Intent in = new Intent(DashboardActivity.this, SOPOOrderListActivity.class);
                in.putExtra("FROM", "PO");
                startActivity(in);
                break;
        }
    }
}
