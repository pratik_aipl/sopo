package com.beta.sopo.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.beta.sopo.BaseActivity;
import com.beta.sopo.R;
import com.beta.sopo.utils.Constant;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        int SPLASH_TIME_OUT = 2500;
        new Handler().postDelayed(() -> {

      /*      if (prefs.getBoolean(Constant.isLogin, false)) {
                Intent i;
                i = new Intent(SplashActivity.this, DashboardActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            } else {*/
                Intent i = new Intent(this, LoginActivity.class);
                startActivity(i);
           // }
            finish();
        }, SPLASH_TIME_OUT);

    }
}
