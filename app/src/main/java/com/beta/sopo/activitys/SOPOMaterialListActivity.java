package com.beta.sopo.activitys;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.beta.sopo.BaseActivity;
import com.beta.sopo.R;
import com.beta.sopo.adpater.MaterialListAdpater;
import com.beta.sopo.listner.EditMaterial;
import com.beta.sopo.listner.RemoveMaterial;
import com.beta.sopo.model.DTPTCItemModel;
import com.beta.sopo.model.MainDataModel;
import com.beta.sopo.model.OrderEntity;
import com.beta.sopo.model.OrderSOPODetails;
import com.beta.sopo.model.SOPOMaterial;
import com.beta.sopo.network.NetworkRequest;
import com.beta.sopo.utils.Constant;
import com.beta.sopo.utils.DecimalFormatLimit;
import com.beta.sopo.utils.L;
import com.bluelinelabs.logansquare.LoganSquare;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class SOPOMaterialListActivity extends BaseActivity implements RemoveMaterial, EditMaterial {

    private static final String TAG = "SOPOMaterialListActivit";
    Dialog materialDialog;
    String From, Type;
    MaterialListAdpater materialListAdpater;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.img_edit)
    ImageView imgEdit;
    @BindView(R.id.img_addd)
    ImageView imgAddd;
    @BindView(R.id.lli)
    LinearLayout lli;
    @BindView(R.id.lin_header)
    LinearLayout linHeader;
    @BindView(R.id.recycler_list)
    RecyclerView material_list;
    @BindView(R.id.edt_remark)
    EditText edtRemark;
    @BindView(R.id.btn_Next)
    Button btnNext;
    @BindView(R.id.lin_btm)
    LinearLayout linBtm;

    List<SOPOMaterial> AddsopoMaterials = new ArrayList<>();
    SOPOMaterial sopoMaterials;
    @BindView(R.id.tv_totalPrice)
    TextView tvTotalPrice;
    MainDataModel mainDataModel;
    OrderEntity orderEntity;
    public ArrayList<String> strMaterialArray = new ArrayList<>();

    UnitAdapter unitAdapter;
    ContainerSizeAdapter containerSizeAdapter;
    List<DTPTCItemModel> UnitList = new ArrayList<>();
    List<DTPTCItemModel> ContainerList = new ArrayList<>();

    String Unit, MaterialID, MaterialName, WeightUnitID, TotalPriceAmount = "", ContainerSize,
            ContainerSizeID;
    Subscription subscription;
    OrderSOPODetails orderSOPODetails;

    String getData, types;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sopomaterial_list);
        ButterKnife.bind(this);


        imgAddd.setVisibility(View.VISIBLE);
        tvTittle.setText("SO ORDERS");

        mainDataModel = (MainDataModel) getIntent().getSerializableExtra(Constant.MaterialData);
        orderEntity = (OrderEntity) getIntent().getSerializableExtra(Constant.OrderEntity);

        material_list.setHasFixedSize(true);
        material_list.setItemAnimator(new DefaultItemAnimator());
        material_list.setLayoutManager(new LinearLayoutManager(this));


        if (getIntent() != null) {
            From = getIntent().getStringExtra("FROM");
            Type = getIntent().getStringExtra(Constant.Type);
        }

        if (From.equalsIgnoreCase("SO")) {
            tvTittle.setText("SO ORDERS");
            getData = prefs.getString(Constant.MaterialDataSO, null);


        } else if (From.equalsIgnoreCase("PO")) {
            tvTittle.setText("PO ORDERS");
            getData = prefs.getString(Constant.MaterialDataPO, null);

        }
        Log.d(TAG, "Material List>>: " + getData);
        types = From;

        if (prefs != null && getData != null) {

            try {
                AddsopoMaterials = LoganSquare.parseList(getData, SOPOMaterial.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
            materialListAdpater = new MaterialListAdpater(this, AddsopoMaterials, "ADD");
            material_list.setAdapter(materialListAdpater);
        } else {
            materialListAdpater = new MaterialListAdpater(this, AddsopoMaterials, "ADD");
            material_list.setAdapter(materialListAdpater);
        }

        TotalPrice();


        if (Type != null)
            if (Type.equalsIgnoreCase("Edit")) {

                orderSOPODetails = (OrderSOPODetails) getIntent().getSerializableExtra(Constant.OrderSOPODetails);
                tvTotalPrice.setText(orderEntity.getCurrencyType() + " " + orderSOPODetails.getTotalOrderAmount());
                TotalPriceAmount = orderSOPODetails.getTotalOrderAmount();
                edtRemark.setText(orderEntity.getRemark());
                AddsopoMaterials = (List<SOPOMaterial>) getIntent().getSerializableExtra(Constant.sopoMaterial);
                materialListAdpater = new MaterialListAdpater(this, AddsopoMaterials, "ADD");
                material_list.setAdapter(materialListAdpater);

            }

        edtRemark.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (types.equalsIgnoreCase("SO")) {
                    prefs.save("RemarkSO", edtRemark.getText().toString());
                } else {
                    prefs.save("RemarkPO", edtRemark.getText().toString());
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        if (prefs != null) {
            if (types.equalsIgnoreCase("SO")) {
                edtRemark.setText(prefs.getString("RemarkSO", ""));

            } else {
                edtRemark.setText(prefs.getString("RemarkPO", ""));

            }

        }

    }

    public void AddMDialog(String From, SOPOMaterial sopoMaterial, int pos) {

        materialDialog = new Dialog(this, R.style.Theme_Dialog);
        materialDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        materialDialog.setCancelable(false);
        materialDialog.setContentView(R.layout.add_material);

        Button btnclose, btnAdd;
        AutoCompleteTextView tvmaterial;
        EditText edtQty, edtUnitPrice, edtTotalPrice, edt_NoOFCont;
        Spinner spn_unit, spn_TypeOfCont;


        btnclose = materialDialog.findViewById(R.id.btn_cancel);
        btnAdd = materialDialog.findViewById(R.id.btn_add);
        tvmaterial = materialDialog.findViewById(R.id.aTv_Material);
        spn_unit = materialDialog.findViewById(R.id.spn_unit);
        spn_TypeOfCont = materialDialog.findViewById(R.id.spn_typeOdCont);

        edt_NoOFCont = materialDialog.findViewById(R.id.edt_noOfcont);
        edtQty = materialDialog.findViewById(R.id.edt_Qty);
        edtUnitPrice = materialDialog.findViewById(R.id.edt_per_unit);
        edtTotalPrice = materialDialog.findViewById(R.id.edt_Total_price);

        edt_NoOFCont.setFilters(new InputFilter[]{new DecimalFormatLimit(5, 2)});

        if (mainDataModel != null) {
            //unit List
            unitAdapter = new UnitAdapter(this, mainDataModel.getUnits());
            spn_unit.setAdapter(unitAdapter);

            containerSizeAdapter = new ContainerSizeAdapter(this, mainDataModel.getContainerSizes());
            spn_TypeOfCont.setAdapter(containerSizeAdapter);

            strMaterialArray.clear();
            for (int i = 0; i < mainDataModel.getMaterials().size(); i++) {
                strMaterialArray.add(mainDataModel.getMaterials().get(i).getMaterial_Name());
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>
                    (this, android.R.layout.select_dialog_item, strMaterialArray);

            tvmaterial.setThreshold(1);
            tvmaterial.setAdapter(adapter);
            tvmaterial.setTextColor(Color.BLACK);
        }


        if (From.equalsIgnoreCase("EDIT")) {
            tvmaterial.setText(sopoMaterial.getMaterialName());
            edtQty.setText(sopoMaterial.getQty());
            edt_NoOFCont.setText(sopoMaterial.getNoOfContainer());
            edtUnitPrice.setText(sopoMaterial.getMaterialPrice());
            edtTotalPrice.setText(sopoMaterial.getTotalPrice());
            btnAdd.setText("SAVE");

            for (int i = 0; i < mainDataModel.getUnits().size(); i++) {
                if (sopoMaterial.getWeightUnitID().equalsIgnoreCase(mainDataModel.getUnits().get(i).getUnitID())) {
                    spn_unit.setSelection(i + 1);
                }
            }
            for (int i = 0; i < mainDataModel.getContainerSizes().size(); i++) {
                if (sopoMaterial.getContainerSizeID().equalsIgnoreCase(mainDataModel.getContainerSizes().get(i).getRecID())) {
                    spn_TypeOfCont.setSelection(i + 1);
                }
            }
        }


        btnclose.setOnClickListener(v -> materialDialog.dismiss());

        edtUnitPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (TextUtils.isEmpty(L.getEditText(edtQty))) {
                    Toast.makeText(SOPOMaterialListActivity.this, "Please Enter Qty", Toast.LENGTH_SHORT).show();
                } else if (edtUnitPrice.getText().toString().trim().length() == 0) {
                    // edtQty.setText("");
                    edtTotalPrice.setText("");
                    Toast.makeText(SOPOMaterialListActivity.this, "Please Enter Unit Price", Toast.LENGTH_SHORT).show();

                } else {
                    Float Tprice = Float.parseFloat((edtQty.getText().toString().trim()));
                    Float Tprices = Float.parseFloat((edtUnitPrice.getText().toString().trim()));

                    edtTotalPrice.setText(String.format("%.2f", (Tprice * Tprices)));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (TextUtils.isEmpty(L.getEditText(edtUnitPrice))) {
                    //Toast.makeText(SOPOMaterialListActivity.this, "Please Enter UNIT Price", Toast.LENGTH_SHORT).show();
                } else if (edtQty.getText().toString().trim().length() == 0) {
                    Toast.makeText(SOPOMaterialListActivity.this, "Please Enter QTY", Toast.LENGTH_SHORT).show();

                } else {
                    Float Tprice = Float.parseFloat((edtQty.getText().toString().trim()));
                    Float Tprices = Float.parseFloat((edtUnitPrice.getText().toString().trim()));

                    edtTotalPrice.setText(String.format("%.2f", (Tprice * Tprices)));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        spn_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    UnitList.clear();
                    UnitList.addAll(mainDataModel.getUnits());
                    Unit = UnitList.get(position - 1).getUnit();
                    WeightUnitID = UnitList.get(position - 1).getUnitID();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spn_TypeOfCont.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    ContainerList.clear();
                    ContainerList.addAll(mainDataModel.getContainerSizes());
                    ContainerSize = ContainerList.get(position - 1).getCSDesc();
                    ContainerSizeID = ContainerList.get(position - 1).getRecID();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        tvmaterial.setOnItemClickListener((parent, view, position, rowId) -> {
            String selection = (String) parent.getItemAtPosition(position);
            Log.d(TAG, "Selection: " + selection);

            for (int i = 0; i < mainDataModel.getMaterials().size(); i++) {
                if (selection.equalsIgnoreCase(mainDataModel.getMaterials().get(i).getMaterial_Name())) {
                    MaterialID = mainDataModel.getMaterials().get(i).getRecID();
                    MaterialName = mainDataModel.getMaterials().get(i).getMaterial_Name();
                    break;
                }
            }


        });

        btnAdd.setOnClickListener(v -> {

            if (TextUtils.isEmpty(tvmaterial.getText().toString())) {
                Toast.makeText(this, "Please Enter Material", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(MaterialID)) {
                Toast.makeText(this, "Please Select Valid Material", Toast.LENGTH_SHORT).show();
            } else if (!MaterialName.equalsIgnoreCase(tvmaterial.getText().toString())) {
                Toast.makeText(this, "Please Select Valid Material", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(L.getEditText(edtQty))) {
                edtQty.setError("Please Enter Qty");
            } else if (spn_unit.getSelectedItemPosition() == 0 || Unit == null) {
                Toast.makeText(this, "Please enter Unit", Toast.LENGTH_SHORT).show();
            }else if (TextUtils.isEmpty(edt_NoOFCont.getText().toString())) {
                Toast.makeText(this, "Please enter No Of Container", Toast.LENGTH_SHORT).show();
            } else if (Float.parseFloat(edt_NoOFCont.getText().toString()) <= 0) {
                Toast.makeText(this, "Please enter Grater then 0", Toast.LENGTH_SHORT).show();
            } else if (spn_TypeOfCont.getSelectedItemPosition() == 0) {
                Toast.makeText(this, "Please enter Container Size", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(L.getEditText(edtUnitPrice))) {
                edtUnitPrice.setError("Please enter PerUnit Price");
            }  else {

                sopoMaterials = new SOPOMaterial();

                if (Type != null && Type.equalsIgnoreCase("Edit")) {
                    sopoMaterials.setOrderID(orderSOPODetails.getOrderID());
                    if (From.equalsIgnoreCase("EDIT")) {
                        sopoMaterials.setMaterialID(sopoMaterial.getMaterialID());
                    } else {
                        sopoMaterials.setMaterialID(MaterialID);
                    }

                } else {
                    sopoMaterials.setOrderID("0");
                    sopoMaterials.setMaterialID(MaterialID);

                }

                sopoMaterials.setCreatedBy(user.getUserID());

                sopoMaterials.setWeightUnitID(WeightUnitID);
                sopoMaterials.setMaterialName(MaterialName);
                sopoMaterials.setOrderMaterialID("0");
                sopoMaterials.setQty(edtQty.getText().toString().trim());
                sopoMaterials.setMaterialPrice(edtUnitPrice.getText().toString().trim());
                sopoMaterials.setTotalPrice(edtTotalPrice.getText().toString().trim());
                sopoMaterials.setUnit(Unit);
                sopoMaterials.setOrderStatus("P");
                sopoMaterials.setContainerSize(ContainerSize);
                sopoMaterials.setContainerSizeID(ContainerSizeID);
                sopoMaterials.setNoOfContainer(edt_NoOFCont.getText().toString().trim());


                if (From.equalsIgnoreCase("EDIT")) {
                    AddsopoMaterials.set(pos, sopoMaterials);
                } else {
                    AddsopoMaterials.add(sopoMaterials);
                }

                materialListAdpater.notifyDataSetChanged();

                orderEntity.setTotalOrderAmount(TotalPriceAmount);
                orderEntity.setRemark(edtRemark.getText().toString().trim());


                TotalPrice();

                if (types.equalsIgnoreCase("SO")) {
                    prefs.save(Constant.MaterialDataSO, (new Gson().toJson(AddsopoMaterials)));
                } else if (types.equalsIgnoreCase("PO")) {
                    prefs.save(Constant.MaterialDataPO, (new Gson().toJson(AddsopoMaterials)));
                }
                Log.d(TAG, "AddMDialog: " + new Gson().toJson(AddsopoMaterials));


                materialDialog.dismiss();

            }
        });


        materialDialog.show();

    }

    @OnClick({R.id.img_back, R.id.img_addd, R.id.btn_Next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.img_addd:
                AddMDialog("FIRST", sopoMaterials, -1);
                break;
            case R.id.btn_Next:
                orderEntity.setTotalOrderAmount(TotalPriceAmount);
                orderEntity.setRemark(edtRemark.getText().toString().trim());
                JSONObject jsonObject = new JSONObject();
                try {
                    JSONArray jsonArray = new JSONArray();
                    /*if (RealmController.with(this).hasMatirial()) {
                        jsonObject.put("orderEntity", new JSONObject(new Gson().toJson( RealmController.with(this).getOrder().toString())));
                        for (int i = 0; i < RealmController.with(this).getMaterial().size(); i++) {
                            JSONObject arr = new JSONObject(new Gson().toJson(AddsopoMaterials.get(i)));
                            jsonArray.put(arr);
                        }
                    } else {*/
                    jsonObject.put("orderEntity", new JSONObject(new Gson().toJson(orderEntity)));
                    for (int i = 0; i < AddsopoMaterials.size(); i++) {
                        JSONObject arr = new JSONObject(new Gson().toJson(AddsopoMaterials.get(i)));
                        jsonArray.put(arr);
                        //   }
                    }


                    jsonObject.put("ordersMaterialEntities", jsonArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (L.isNetworkAvailable(this)) {
                    if (AddsopoMaterials.size() != 0) {
                        if (Type != null && Type.equalsIgnoreCase("Edit")) {

                            UpdateOrder(jsonObject.toString());
                        } else {
                            UploadOrder(jsonObject.toString());
                        }
                    } else {
                        Toast.makeText(this, "Please Add Material Details", Toast.LENGTH_SHORT).show();
                    }
                }

                break;
        }
    }


    private void UploadOrder(String s) {

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getOrderCreate(From.equalsIgnoreCase("SO") ? "CreateSO" : "CreatePO", s), (data) -> {
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.getBoolean("Status")) {
                        // Toast.makeText(this, jsonResponse.getString("UMessage"), Toast.LENGTH_SHORT).show();
                        AddsopoMaterials.clear();

                        new AlertDialog.Builder(this)
                                .setMessage(jsonResponse.getString("UMessage"))
                                .setPositiveButton("OK", (dialog, which) -> {

                                    if (types.equalsIgnoreCase("SO")) {
                                        prefs.save(Constant.OrderEntitySO, null);
                                        prefs.save(Constant.MaterialDataSO, null);
                                        prefs.save("RemarkSO", null);
                                    } else {
                                        prefs.save(Constant.OrderEntityPO, null);
                                        prefs.save(Constant.MaterialDataPO, null);
                                        prefs.save("RemarkPO", null);
                                    }
                                    startActivity(new Intent(SOPOMaterialListActivity.this, SOPOOrderListActivity.class)
                                            .putExtra("FROM", From)
                                            .putExtra("Back", "Dash")
                                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                                                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                                    Intent.FLAG_ACTIVITY_SINGLE_TOP |
                                                    Intent.FLAG_ACTIVITY_CLEAR_TOP));


                                })
                                .show();


//                        prefs.save(Constant.OrderEntity, null);
                        //  prefs.save(Constant.MaterialData, null);
                        //  L.alert(SOPOMaterialListActivity.this, jsonResponse.getString("UMessage"));

                    } else {
                        L.alert(SOPOMaterialListActivity.this, jsonResponse.getString("UMessage"));
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                showProgress(false);
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    private void UpdateOrder(String s) {


        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getUpdateOrder(From.equalsIgnoreCase("SO") ? "UpdateSO" : "UpdatePO", s), (data) -> {
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.getBoolean("Status")) {
                        AddsopoMaterials.clear();

                        new AlertDialog.Builder(this)
                                .setMessage(jsonResponse.getString("UMessage"))
                                .setPositiveButton("OK", (dialog, which) -> {

                                    if (types.equalsIgnoreCase("SO")) {
                                        prefs.save(Constant.OrderEntitySO, null);
                                        prefs.save(Constant.MaterialDataSO, null);
                                        prefs.save("RemarkSO", null);


                                    } else {
                                        prefs.save(Constant.OrderEntityPO, null);
                                        prefs.save(Constant.MaterialDataPO, null);
                                        prefs.save("RemarkPO", null);

                                    }
                                    // prefs.save(Constant.OrderEntity, null);
                                    //prefs.save(Constant.MaterialData, null);

                                    startActivity(new Intent(SOPOMaterialListActivity.this, SOPOOrderListActivity.class)
                                            .putExtra("FROM", From)
                                            .putExtra("Back", "Dash")
                                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                                                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                                    Intent.FLAG_ACTIVITY_SINGLE_TOP |
                                                    Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                })
                                .show();


                    } else {
                        L.alert(SOPOMaterialListActivity.this, jsonResponse.getString("UMessage"));
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                showProgress(false);
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    public void TotalPrice() {
        if (AddsopoMaterials.size() != 0) {
            float TPrice = 0;
            for (int i = 0; i < AddsopoMaterials.size(); i++) {
                TPrice = TPrice + Float.parseFloat(AddsopoMaterials.get(i).getTotalPrice());
            }
            tvTotalPrice.setText(orderEntity.getCurrencyType() + " " + TPrice);
            edtRemark.setText(orderEntity.getRemark());
            TotalPriceAmount = String.valueOf(TPrice);
            orderEntity.setTotalOrderAmount(TotalPriceAmount);
            prefs.save(Constant.OrderEntity, (new Gson().toJson(orderEntity)));
        } else {
            tvTotalPrice.setText(orderEntity.getCurrencyType() + " " + "0000");
        }
    }

    @Override
    public void onRemoveProduct(SOPOMaterial sopoMaterial, int pos) {
        AddsopoMaterials.remove(pos);
        TotalPrice();
        materialListAdpater.notifyDataSetChanged();
        if (types.equalsIgnoreCase("SO")) {
            prefs.save(Constant.MaterialDataSO, (new Gson().toJson(AddsopoMaterials)));
        } else {
            prefs.save(Constant.MaterialDataPO, (new Gson().toJson(AddsopoMaterials)));
        }
    }

    @Override
    public void onEditProduct(SOPOMaterial sopoMaterial, int pos) {
        AddMDialog("EDIT", sopoMaterial, pos);

    }

    class UnitAdapter extends BaseAdapter {
        List<DTPTCItemModel> unitlist;
        LayoutInflater inflter;
        Context context;

        public UnitAdapter(Context context, List<DTPTCItemModel> unitlists) {
            this.context = context;
            this.unitlist = unitlists;
            inflter = (LayoutInflater.from(context));
        }

        @Override
        public int getCount() {
            return unitlist.size() + 1;
        }

        @Override
        public Object getItem(int position) {
            return unitlist.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = inflter.inflate(R.layout.custom_spinner_items, null);
            TextView names = convertView.findViewById(R.id.tv_spinner);
            if (position == 0) {
                names.setText("Select Unit");
                names.setTextColor(Color.GRAY);
            } else {
                DTPTCItemModel dtptcCMPYModel = unitlist.get(position - 1);
                names.setText(dtptcCMPYModel.getUnit());
                names.setTextColor(Color.BLACK);
            }
            return convertView;
        }


    }

    class ContainerSizeAdapter extends BaseAdapter {
        List<DTPTCItemModel> ContainerSizelist;
        LayoutInflater inflter;
        Context context;

        public ContainerSizeAdapter(Context context, List<DTPTCItemModel> ContainerSizelists) {
            this.context = context;
            this.ContainerSizelist = ContainerSizelists;
            inflter = (LayoutInflater.from(context));
        }

        @Override
        public int getCount() {
            return ContainerSizelist.size() + 1;
        }

        @Override
        public Object getItem(int position) {
            return ContainerSizelist.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = inflter.inflate(R.layout.custom_spinner_items, null);
            TextView names = convertView.findViewById(R.id.tv_spinner);
            if (position == 0) {
                names.setText("Select Container Size");
                names.setTextColor(Color.GRAY);
            } else {
                DTPTCItemModel dtptcCMPYModel = ContainerSizelist.get(position - 1);
                names.setText(dtptcCMPYModel.getCSDesc());
                names.setTextColor(Color.BLACK);
            }
            return convertView;
        }


    }
}
