package com.beta.sopo.activitys;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.beta.sopo.BaseActivity;
import com.beta.sopo.R;
import com.beta.sopo.network.NetworkRequest;
import com.beta.sopo.utils.L;
import com.beta.sopo.utils.Validation;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class ForgotPassword extends BaseActivity {

    @BindView(R.id.edt_email)
    EditText edtEmail;
    @BindView(R.id.btn_send)
    Button btnSend;
    @BindView(R.id.img_back)
    ImageView imgBack;

    Subscription subscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);

    }


    @OnClick({R.id.img_back, R.id.btn_send})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.btn_send:
                validation();
                break;
        }
    }

    public void validation() {
        if (!Validation.isValidEmail(L.getEditText(edtEmail))) {
            edtEmail.setError("Check Email ID");
        } else {
            if (L.isNetworkAvailable(ForgotPassword.this)) {
                ForgotPaswd();
            }
        }
    }

    private void ForgotPaswd() {
        Map<String, String> map = new HashMap<>();
        map.put("UserEmail", edtEmail.getText().toString().trim());

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.ForgotPassword(map), (data) -> {
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.getBoolean("Status")) {
                        //  Toast.makeText(this, jsonResponse.getString("UMessage"), Toast.LENGTH_SHORT).show();
                        alert(jsonResponse.getString("UMessage"), jsonResponse.getString("Password"));
                        edtEmail.setText(" ");
                        // onBackPressed();
                    } else {
                        L.alert(ForgotPassword.this, jsonResponse.getString("UMessage"));
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                showProgress(false);
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    public void alert(String msg, String paswd) {
        new AlertDialog.Builder(this)
                .setMessage(msg + " " + "' " + paswd + " '")
                .setPositiveButton("OK", null)
                .setNegativeButton("COPY", (dialog, which) -> {
                    ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("Copy", paswd);
                    clipboard.setPrimaryClip(clip);

                })
                .show();
    }
}
