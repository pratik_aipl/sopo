package com.beta.sopo.adpater;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.beta.sopo.R;
import com.beta.sopo.activitys.OrderDetailsActivity;
import com.beta.sopo.activitys.SOPOMaterialListActivity;
import com.beta.sopo.listner.EditMaterial;
import com.beta.sopo.listner.RemoveMaterial;
import com.beta.sopo.model.SOPOMaterial;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MaterialListAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "MaterialListAdpater";
    Context context;
    List<SOPOMaterial> sopoMaterial;
    String From;
    RemoveMaterial removeMaterial;
    EditMaterial editMaterial;


    public MaterialListAdpater(SOPOMaterialListActivity context, List<SOPOMaterial> sopoMaterials, String from) {
        this.context = context;
        this.sopoMaterial = sopoMaterials;
        this.From = from;
        this.removeMaterial = context;
        this.editMaterial = context;
    }

    public MaterialListAdpater(OrderDetailsActivity context, List<SOPOMaterial> sopoMaterials, String from) {
        this.context = context;
        this.sopoMaterial = sopoMaterials;
        this.From = from;

    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.sopo_meterial_row_item, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        SOPOMaterial material = sopoMaterial.get(position);

        holder.tvMaterialName.setText(material.getMaterialName());
        holder.tvMQty.setText(material.getQty());
        holder.tvNoOfContainer.setText(material.getNoOfContainer());
        holder.tvContSize.setText(material.getContainerSize());
        holder.tvUnit.setText(material.getUnit());
        holder.tvPUPrice.setText(material.getMaterialPrice());
        holder.tvTPrice.setText(material.getTotalPrice());

        if (From.equalsIgnoreCase("DETAIL")) {
            holder.imgDlt.setVisibility(View.GONE);
            holder.imgEdit.setVisibility(View.GONE);
        }

        holder.imgDlt.setOnClickListener(v -> {

            new AlertDialog.Builder(context)
                    .setMessage("Are you sure you want to delete this Material?")
                    .setPositiveButton("YES", (dialog, which) -> {
                        removeMaterial.onRemoveProduct(material, position);
                    })
                    .setNegativeButton("NO", null)
                    .show();

        });
        holder.imgEdit.setOnClickListener(v -> {
            editMaterial.onEditProduct(material, position);

        });

    }

    @Override
    public int getItemCount() {

        return sopoMaterial.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_MaterialName)
        TextView tvMaterialName;
        @BindView(R.id.img_edit)
        ImageView imgEdit;
        @BindView(R.id.img_dlt)
        ImageView imgDlt;
        @BindView(R.id.tv_MQty)
        TextView tvMQty;
        @BindView(R.id.tv_NoOfContainer)
        TextView tvNoOfContainer;
        @BindView(R.id.tv_ContSize)
        TextView tvContSize;
        @BindView(R.id.tv_Unit)
        TextView tvUnit;
        @BindView(R.id.tv_PUPrice)
        TextView tvPUPrice;
        @BindView(R.id.tv_TPrice)
        TextView tvTPrice;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}