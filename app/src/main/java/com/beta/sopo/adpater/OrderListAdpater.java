package com.beta.sopo.adpater;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.beta.sopo.R;
import com.beta.sopo.activitys.OrderDetailsActivity;
import com.beta.sopo.activitys.SOPOOrderListActivity;
import com.beta.sopo.listner.SearchDataList;
import com.beta.sopo.model.OrderSOPO;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class OrderListAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    private static final String TAG = "OrderListAdpater";
    Context context;
    List<OrderSOPO> orderSOPO;
    List<OrderSOPO> mFilteredList;

    SearchDataList searchDataList;
    String From;

    public OrderListAdpater(SOPOOrderListActivity context, List<OrderSOPO> orderSOPOS, String from) {
        this.context = context;
        this.orderSOPO = orderSOPOS;
        this.mFilteredList = orderSOPOS;
        this.searchDataList = context;
        this.From = from;
    }

    public OrderListAdpater(Context context, List<OrderSOPO> orderSOPOS, String from) {
        this.context = context;
        this.orderSOPO = orderSOPOS;
        this.mFilteredList = orderSOPOS;
        this.From = from;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.sopo_orderlist_row_item, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        OrderSOPO sopo = mFilteredList.get(position);

        holder.tvOrderId.setText(sopo.getOrderNo());
        holder.tvCmpnyname.setText(sopo.getCompanyName());
        holder.tvDate.setText(sopo.getOrderDate());
        holder.tvStatus.setText(sopo.getOrderStatus());
        holder.tvSupplier.setText(sopo.getPartyName());
        holder.tvTotalamt.setText(sopo.getCurrencyName() + " " + sopo.getTotalOrderAmount());

        holder.tvchngname.setText(From.equalsIgnoreCase("SO") ? "Customer" : "Supplier");

        holder.itemView.setOnClickListener(v -> context.startActivity(new Intent(context, OrderDetailsActivity.class)
                .putExtra("ID", sopo.getOrderID()).putExtra("Type", From)));
    }

    @Override
    public int getItemCount() {

        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                String[] filterData = charString.split(",");

                Log.d(TAG, "filterData>: " + filterData[0] + filterData[1] + filterData[2]);

                if (charString.isEmpty()) {
                    mFilteredList.addAll(orderSOPO);
                } else {
                    List<OrderSOPO> filteredList = new ArrayList<>();
                    for (OrderSOPO row : orderSOPO) {

                        if (filterData.length > 2) {

                            if (filterData[2].equalsIgnoreCase("All")) {
                                if (row.getCompanyName().toLowerCase().contains(filterData[0].toLowerCase())
                                        && row.getPartyName().toLowerCase().contains(filterData[1].toLowerCase())) {
                                    filteredList.add(row);
                                }
                            } else {
                                if (row.getCompanyName().toLowerCase().contains(filterData[0].toLowerCase())
                                        && row.getPartyName().toLowerCase().contains(filterData[1].toLowerCase())
                                        && row.getOrderStatus().toLowerCase().contains(filterData[2].toLowerCase())) {
                                    filteredList.add(row);
                                }
                            }

                        } else if (filterData.length > 1) {
                            if (row.getCompanyName().toLowerCase().contains(filterData[0].toLowerCase())
                                    && row.getPartyName().toLowerCase().contains(filterData[1].toLowerCase())) {
                                filteredList.add(row);
                            }
                        } else {
                            if (row.getOrderStatus().toLowerCase().contains(filterData[2].toLowerCase())) {
                                filteredList.add(row);
                            }

                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                if (mFilteredList.size() == 0) {
                    searchDataList.onListsize(mFilteredList.size());
                }
                mFilteredList = (ArrayList<OrderSOPO>) filterResults.values;
                notifyDataSetChanged();
                Log.d(TAG, "publishResults: " + mFilteredList.size());
            }
        };
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_cmpnyname)
        TextView tvCmpnyname;
        @BindView(R.id.tv_orderId)
        TextView tvOrderId;
        @BindView(R.id.tv_status)
        TextView tvStatus;
        @BindView(R.id.tv_Date)
        TextView tvDate;
        @BindView(R.id.tv_supplier)
        TextView tvSupplier;
        @BindView(R.id.tv_totalamt)
        TextView tvTotalamt;
        @BindView(R.id.tv_SOPOChngname)
        TextView tvchngname;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}