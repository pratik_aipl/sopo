package com.beta.sopo.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class DTPTCMainModel implements Serializable {

    @JsonField
    List<DTPTCItemModel> companies;
    @JsonField
    List<DTPTCItemModel> paymentTerms;
    @JsonField
    List<DTPTCItemModel> deliveryTerms;
    @JsonField
    List<DTPTCItemModel> currencies;

    public List<DTPTCItemModel> getCompanies() {
        return companies;
    }

    public void setCompanies(List<DTPTCItemModel> companies) {
        this.companies = companies;
    }

    public List<DTPTCItemModel> getPaymentTerms() {
        return paymentTerms;
    }

    public void setPaymentTerms(List<DTPTCItemModel> paymentTerms) {
        this.paymentTerms = paymentTerms;
    }

    public List<DTPTCItemModel> getDeliveryTerms() {
        return deliveryTerms;
    }

    public void setDeliveryTerms(List<DTPTCItemModel> deliveryTerms) {
        this.deliveryTerms = deliveryTerms;
    }

    public List<DTPTCItemModel> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<DTPTCItemModel> currencies) {
        this.currencies = currencies;
    }
}
