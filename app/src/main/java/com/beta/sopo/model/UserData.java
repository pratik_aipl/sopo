package com.beta.sopo.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class UserData implements Serializable {

    @JsonField
    String UserID;

    @JsonField
    UserEntity userEntity;
    @JsonField
    TokenEntity tokenEntity;

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    public TokenEntity getTokenEntity() {
        return tokenEntity;
    }

    public void setTokenEntity(TokenEntity tokenEntity) {
        this.tokenEntity = tokenEntity;
    }

    public UserData() {
    }


}
