package com.beta.sopo.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class AllCountry implements Serializable {

    @JsonField
    String RecID;
    @JsonField
    String CountryShortName;
    @JsonField
    String CountryName;
    @JsonField
    String RegionID;

    public String getRecID() {
        return RecID;
    }

    public void setRecID(String recID) {
        RecID = recID;
    }

    public String getCountryShortName() {
        return CountryShortName;
    }

    public void setCountryShortName(String countryShortName) {
        CountryShortName = countryShortName;
    }

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public String getRegionID() {
        return RegionID;
    }

    public void setRegionID(String regionID) {
        RegionID = regionID;
    }
}
