package com.beta.sopo.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class DivisionModel implements Serializable {

    @JsonField
    List<DTPTCItemModel> divisions;

    public List<DTPTCItemModel> getDivisions() {
        return divisions;
    }

    public void setDivisions(List<DTPTCItemModel> divisions) {
        this.divisions = divisions;
    }
}
