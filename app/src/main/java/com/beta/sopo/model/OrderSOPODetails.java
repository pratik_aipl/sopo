package com.beta.sopo.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class OrderSOPODetails implements Serializable {

    @JsonField
    String CompanyName;
    @JsonField
    String DivisionName;
    @JsonField
    String SubDivisionName;
    @JsonField
    String PartyName;
    @JsonField
    String CurrencyName;
    @JsonField
    String PaymentTerm;
    @JsonField
    String DeliveryTerm;
    @JsonField
    String OrderID;

    @JsonField
    String OrderType;
    @JsonField
    String OrderNo;
    @JsonField
    String LAOrderNo;
    @JsonField
    String OrderDate;
    @JsonField
    String CompanyID;
    @JsonField
    String DivisionID;
    @JsonField
    String SubDivisionID;
    @JsonField
    String PartyID;
    @JsonField
    String CurrencyID;
    @JsonField
    String PaymentTermID;
    @JsonField
    String DeliveryTermID;
    @JsonField
    String Remark;
    @JsonField
    String TotalOrderAmount;
    @JsonField
    String OrderStatus;
    @JsonField
    String LoadingType;
    @JsonField
    String CommissionID;
    @JsonField
    String CommGroupName;

    public String getCommGroupName() {
        return CommGroupName;
    }

    public void setCommGroupName(String commGroupName) {
        CommGroupName = commGroupName;
    }

    public String getLoadingType() {
        return LoadingType;
    }

    public void setLoadingType(String loadingType) {
        LoadingType = loadingType;
    }

    public String getCommissionID() {
        return CommissionID;
    }

    public void setCommissionID(String commissionID) {
        CommissionID = commissionID;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getDivisionName() {
        return DivisionName;
    }

    public void setDivisionName(String divisionName) {
        DivisionName = divisionName;
    }

    public String getSubDivisionName() {
        return SubDivisionName;
    }

    public void setSubDivisionName(String subDivisionName) {
        SubDivisionName = subDivisionName;
    }

    public String getPartyName() {
        return PartyName;
    }

    public void setPartyName(String partyName) {
        PartyName = partyName;
    }

    public String getCurrencyName() {
        return CurrencyName;
    }

    public void setCurrencyName(String currencyName) {
        CurrencyName = currencyName;
    }

    public String getPaymentTerm() {
        return PaymentTerm;
    }

    public void setPaymentTerm(String paymentTerm) {
        PaymentTerm = paymentTerm;
    }

    public String getDeliveryTerm() {
        return DeliveryTerm;
    }

    public void setDeliveryTerm(String deliveryTerm) {
        DeliveryTerm = deliveryTerm;
    }

    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getOrderType() {
        return OrderType;
    }

    public void setOrderType(String orderType) {
        OrderType = orderType;
    }

    public String getOrderNo() {
        return OrderNo;
    }

    public void setOrderNo(String orderNo) {
        OrderNo = orderNo;
    }

    public String getLAOrderNo() {
        return LAOrderNo;
    }

    public void setLAOrderNo(String LAOrderNo) {
        this.LAOrderNo = LAOrderNo;
    }

    public String getOrderDate() {
        return OrderDate;
    }

    public void setOrderDate(String orderDate) {
        OrderDate = orderDate;
    }

    public String getCompanyID() {
        return CompanyID;
    }

    public void setCompanyID(String companyID) {
        CompanyID = companyID;
    }

    public String getDivisionID() {
        return DivisionID;
    }

    public void setDivisionID(String divisionID) {
        DivisionID = divisionID;
    }

    public String getSubDivisionID() {
        return SubDivisionID;
    }

    public void setSubDivisionID(String subDivisionID) {
        SubDivisionID = subDivisionID;
    }

    public String getPartyID() {
        return PartyID;
    }

    public void setPartyID(String partyID) {
        PartyID = partyID;
    }

    public String getCurrencyID() {
        return CurrencyID;
    }

    public void setCurrencyID(String currencyID) {
        CurrencyID = currencyID;
    }

    public String getPaymentTermID() {
        return PaymentTermID;
    }

    public void setPaymentTermID(String paymentTermID) {
        PaymentTermID = paymentTermID;
    }

    public String getDeliveryTermID() {
        return DeliveryTermID;
    }

    public void setDeliveryTermID(String deliveryTermID) {
        DeliveryTermID = deliveryTermID;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public String getTotalOrderAmount() {
        return TotalOrderAmount;
    }

    public void setTotalOrderAmount(String totalOrderAmount) {
        TotalOrderAmount = totalOrderAmount;
    }

    public String getOrderStatus() {
        return OrderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        OrderStatus = orderStatus;
    }
}
