package com.beta.sopo.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class OrderEntity implements Serializable {

    @JsonField
    String OrderID;
    @JsonField
    String OrderType;
    @JsonField
    String OrderDate;
    @JsonField
    String CompanyID;
    @JsonField
    String TotalOrderAmount;
    @JsonField
    String OrderNo;
    @JsonField
    String Remark;
    @JsonField
    String CurrencyID;
    @JsonField
    String OrderStatus;
    @JsonField
    String DivisionID;
    @JsonField
    String SubDivisionID;
    @JsonField
    String PartyID;
    @JsonField
    String PaymentTermID;
    @JsonField
    String DeliveryTermID;
    @JsonField
    String CreatedBy;
    @JsonField
    String CurrencyType;
    @JsonField
    String CommissionID;
    @JsonField
    String LoadingType;

    public String getLoadingType() {
        return LoadingType;
    }

    public void setLoadingType(String loadingType) {
        LoadingType = loadingType;
    }

    public String getCommissionID() {
        return CommissionID;
    }

    public void setCommissionID(String commissionID) {
        CommissionID = commissionID;
    }

    public String getCurrencyType() {
        return CurrencyType;
    }

    public void setCurrencyType(String currencyType) {
        CurrencyType = currencyType;
    }

    public String getOrderStatus() {
        return OrderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        OrderStatus = orderStatus;
    }

    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getOrderType() {
        return OrderType;
    }

    public void setOrderType(String orderType) {
        OrderType = orderType;
    }

    public String getOrderDate() {
        return OrderDate;
    }

    public void setOrderDate(String orderDate) {
        OrderDate = orderDate;
    }

    public String getCompanyID() {
        return CompanyID;
    }

    public void setCompanyID(String companyID) {
        CompanyID = companyID;
    }

    public String getTotalOrderAmount() {
        return TotalOrderAmount;
    }

    public void setTotalOrderAmount(String totalOrderAmount) {
        TotalOrderAmount = totalOrderAmount;
    }

    public String getOrderNo() {
        return OrderNo;
    }

    public void setOrderNo(String orderNo) {
        OrderNo = orderNo;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public String getCurrencyID() {
        return CurrencyID;
    }

    public void setCurrencyID(String currencyID) {
        CurrencyID = currencyID;
    }

    public String getDivisionID() {
        return DivisionID;
    }

    public void setDivisionID(String divisionID) {
        DivisionID = divisionID;
    }

    public String getSubDivisionID() {
        return SubDivisionID;
    }

    public void setSubDivisionID(String subDivisionID) {
        SubDivisionID = subDivisionID;
    }

    public String getPartyID() {
        return PartyID;
    }

    public void setPartyID(String partyID) {
        PartyID = partyID;
    }

    public String getPaymentTermID() {
        return PaymentTermID;
    }

    public void setPaymentTermID(String paymentTermID) {
        PaymentTermID = paymentTermID;
    }

    public String getDeliveryTermID() {
        return DeliveryTermID;
    }

    public void setDeliveryTermID(String deliveryTermID) {
        DeliveryTermID = deliveryTermID;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }
}
