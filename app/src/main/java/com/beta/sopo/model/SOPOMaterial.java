package com.beta.sopo.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class SOPOMaterial implements Serializable {


    @JsonField
    String MaterialName;
    @JsonField
    String Unit;
    @JsonField
    String OrderMaterialID;
    @JsonField
    String OrderID;
    @JsonField
    String MaterialID;
    @JsonField
    String Qty;
    @JsonField
    String MaterialPrice;
    @JsonField
    String TotalPrice;
    @JsonField
    String OrderStatus;
    @JsonField
    String WeightUnitID;
    @JsonField
    String CreatedBy;
    @JsonField
    String ContainerSizeID;
    @JsonField
    String ContainerSize;
    @JsonField
    String NoOfContainer;

    public String getNoOfContainer() {
        return NoOfContainer;
    }

    public void setNoOfContainer(String noOfContainer) {
        NoOfContainer = noOfContainer;
    }

    public String getContainerSizeID() {
        return ContainerSizeID;
    }

    public void setContainerSizeID(String containerSizeID) {
        ContainerSizeID = containerSizeID;
    }

    public String getContainerSize() {
        return ContainerSize;
    }

    public void setContainerSize(String containerSize) {
        ContainerSize = containerSize;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getWeightUnitID() {
        return WeightUnitID;
    }

    public void setWeightUnitID(String weightUnitID) {
        WeightUnitID = weightUnitID;
    }

    public String getMaterialName() {
        return MaterialName;
    }

    public void setMaterialName(String materialName) {
        MaterialName = materialName;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String unit) {
        Unit = unit;
    }

    public String getOrderMaterialID() {
        return OrderMaterialID;
    }

    public void setOrderMaterialID(String orderMaterialID) {
        OrderMaterialID = orderMaterialID;
    }

    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getMaterialID() {
        return MaterialID;
    }

    public void setMaterialID(String materialID) {
        MaterialID = materialID;
    }

    public String getQty() {
        return Qty;
    }

    public void setQty(String qty) {
        Qty = qty;
    }

    public String getMaterialPrice() {
        return MaterialPrice;
    }

    public void setMaterialPrice(String materialPrice) {
        MaterialPrice = materialPrice;
    }

    public String getTotalPrice() {
        return TotalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        TotalPrice = totalPrice;
    }

    public String getOrderStatus() {
        return OrderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        OrderStatus = orderStatus;
    }
}
