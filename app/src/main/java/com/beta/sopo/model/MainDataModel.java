package com.beta.sopo.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class MainDataModel implements Serializable {

    @JsonField
    List<DTPTCItemModel> subDivisions;
    @JsonField
    List<DTPTCItemModel> materials;
    @JsonField
    List<DTPTCItemModel> suppliers;
    @JsonField
    List<DTPTCItemModel> customers;
    @JsonField
    List<DTPTCItemModel> units;
    @JsonField
    List<DTPTCItemModel> containerSizes;

    public List<DTPTCItemModel> getContainerSizes() {
        return containerSizes;
    }

    public void setContainerSizes(List<DTPTCItemModel> containerSizes) {
        this.containerSizes = containerSizes;
    }

    public List<DTPTCItemModel> getSubDivisions() {
        return subDivisions;
    }

    public void setSubDivisions(List<DTPTCItemModel> subDivisions) {
        this.subDivisions = subDivisions;
    }

    public List<DTPTCItemModel> getMaterials() {
        return materials;
    }

    public void setMaterials(List<DTPTCItemModel> materials) {
        this.materials = materials;
    }

    public List<DTPTCItemModel> getSuppliers() {
        return suppliers;
    }

    public void setSuppliers(List<DTPTCItemModel> suppliers) {
        this.suppliers = suppliers;
    }

    public List<DTPTCItemModel> getCustomers() {
        return customers;
    }

    public void setCustomers(List<DTPTCItemModel> customers) {
        this.customers = customers;
    }

    public List<DTPTCItemModel> getUnits() {
        return units;
    }

    public void setUnits(List<DTPTCItemModel> units) {
        this.units = units;
    }
}
