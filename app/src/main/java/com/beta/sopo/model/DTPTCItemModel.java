package com.beta.sopo.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class DTPTCItemModel implements Serializable {

    @JsonField
    String RecID;
    @JsonField
    String CompanyName;
    @JsonField
    String CompanyCode;
    @JsonField
    String UCID;
    @JsonField
    String PTDesc;
    @JsonField
    String CurCode;
    @JsonField
    String DivisionName;
    @JsonField
    String SubDivisionName;

    @JsonField
    String Material_Name;
    @JsonField
    String Material_Description;
    @JsonField
    String MaterialTypeID;

    @JsonField
    String UnitID;
    @JsonField
    String Unit;

    @JsonField
    String CSDesc;

    @JsonField
    String CommGroupName;

    public String getCSDesc() {
        return CSDesc;
    }

    public void setCSDesc(String CSDesc) {
        this.CSDesc = CSDesc;
    }

    public String getCommGroupName() {
        return CommGroupName;
    }

    public void setCommGroupName(String commGroupName) {
        CommGroupName = commGroupName;
    }

    public String getMaterial_Name() {
        return Material_Name;
    }

    public void setMaterial_Name(String material_Name) {
        Material_Name = material_Name;
    }

    public String getMaterial_Description() {
        return Material_Description;
    }

    public void setMaterial_Description(String material_Description) {
        Material_Description = material_Description;
    }

    public String getMaterialTypeID() {
        return MaterialTypeID;
    }

    public void setMaterialTypeID(String materialTypeID) {
        MaterialTypeID = materialTypeID;
    }

    public String getUnitID() {
        return UnitID;
    }

    public void setUnitID(String unitID) {
        UnitID = unitID;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String unit) {
        Unit = unit;
    }

    public String getSubDivisionName() {
        return SubDivisionName;
    }

    public void setSubDivisionName(String subDivisionName) {
        SubDivisionName = subDivisionName;
    }

    public String getDivisionName() {
        return DivisionName;
    }

    public void setDivisionName(String divisionName) {
        DivisionName = divisionName;
    }

    public String getRecID() {
        return RecID;
    }

    public void setRecID(String recID) {
        RecID = recID;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getCompanyCode() {
        return CompanyCode;
    }

    public void setCompanyCode(String companyCode) {
        CompanyCode = companyCode;
    }

    public String getUCID() {
        return UCID;
    }

    public void setUCID(String UCID) {
        this.UCID = UCID;
    }

    public String getPTDesc() {
        return PTDesc;
    }

    public void setPTDesc(String PTDesc) {
        this.PTDesc = PTDesc;
    }

    public String getCurCode() {
        return CurCode;
    }

    public void setCurCode(String curCode) {
        CurCode = curCode;
    }
}
