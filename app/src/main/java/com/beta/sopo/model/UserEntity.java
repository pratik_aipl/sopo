package com.beta.sopo.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class UserEntity implements Serializable {

    @JsonField
    String UserName;
    @JsonField
    String UserEmail;
    @JsonField
    String IsSOAllowed;
    @JsonField
    String IsPOAllowed;
    @JsonField
    int IsActive;

    public String getIsSOAllowed() {
        return IsSOAllowed;
    }

    public void setIsSOAllowed(String isSOAllowed) {
        IsSOAllowed = isSOAllowed;
    }

    public String getIsPOAllowed() {
        return IsPOAllowed;
    }

    public void setIsPOAllowed(String isPOAllowed) {
        IsPOAllowed = isPOAllowed;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getUserEmail() {
        return UserEmail;
    }

    public void setUserEmail(String userEmail) {
        UserEmail = userEmail;
    }

    public int getIsActive() {
        return IsActive;
    }

    public void setIsActive(int isActive) {
        IsActive = isActive;
    }

    public UserEntity() {
    }


}
