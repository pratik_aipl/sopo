package com.beta.sopo.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class CommissionAgentModel implements Serializable {

    @JsonField
    List<DTPTCItemModel> commissions;

    public List<DTPTCItemModel> getCommissions() {
        return commissions;
    }

    public void setCommissions(List<DTPTCItemModel> commissions) {
        this.commissions = commissions;
    }
}
