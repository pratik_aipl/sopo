package com.beta.sopo.listner;


import com.beta.sopo.model.SOPOMaterial;

public interface RemoveMaterial {
    void onRemoveProduct(SOPOMaterial sopoMaterial, int pos);
}
