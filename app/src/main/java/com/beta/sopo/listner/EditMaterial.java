package com.beta.sopo.listner;


import com.beta.sopo.model.SOPOMaterial;

public interface EditMaterial {
    void onEditProduct(SOPOMaterial sopoMaterial, int pos);
}
