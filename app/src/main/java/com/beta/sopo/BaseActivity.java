package com.beta.sopo;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.beta.sopo.model.UserData;
import com.beta.sopo.network.RestAPIBuilder;
import com.beta.sopo.network.RestApi;
import com.beta.sopo.utils.Constant;
import com.beta.sopo.utils.Prefs;
import com.google.gson.Gson;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.lang.reflect.Type;


public class BaseActivity extends AppCompatActivity {

    protected RestApi restApi;
    public Prefs prefs;
    public UserData user;
    protected Gson gson;
    public Type type;

    ProgressDialog progressDialog;
    public RxPermissions rxPermissions;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rxPermissions = new RxPermissions(this);
        restApi = RestAPIBuilder.buildRetrofitService();
        prefs = Prefs.with(this);
        gson = new Gson();
        // type = new TypeToken<List<ProductsListModel>>(){}.getType();
        user = gson.fromJson(prefs.getString(Constant.UserData, ""), UserData.class);
    }

    public void showProgress(boolean isShow) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading...");
        }
        progressDialog.setCancelable(false);
        if (isShow) {
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }
}
