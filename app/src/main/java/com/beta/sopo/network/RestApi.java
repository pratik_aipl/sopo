package com.beta.sopo.network;


import java.util.Map;

import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import rx.Observable;

public interface RestApi {

    @FormUrlEncoded
    @POST("Register/NewRegister")
    Observable<Response<String>> getRegister(@FieldMap Map<String, String> stringMap);

    @POST("login")
    Observable<Response<String>> getLogin(@Header("Authorization") String auth);

    @FormUrlEncoded
    @POST("Users/ForgotPassword")
    Observable<Response<String>> ForgotPassword(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("orders/{Type}")
    Observable<Response<String>> getOrderSOPO(@Path(value = "Type") String Type, @FieldMap Map<String, String> stringMap);


    @Headers("Content-Type:application/json")
    @POST("Orders/{Type}")
    Observable<Response<String>> getOrderCreate(@Path(value = "Type") String Type, @Body String stringMap);

    @Headers("Content-Type:application/json")
    @PUT("Orders/{Type}")
    Observable<Response<String>> getUpdateOrder(@Path(value = "Type") String Type, @Body String stringMap);

    @GET("orders/{Type}/{OrderID}")
    Observable<Response<String>> getOrderSOPODetails(@Path(value = "Type") String Type, @Path(value = "OrderID") int OrderID);

    @DELETE("Orders/{Type}/{OrderID}")
    Observable<Response<String>> DeleteOrder(@Path(value = "Type") String Type, @Path(value = "OrderID") int OrderID);

    @GET("Masters/GetGroup1/{UserID}/{OrderType}")
    Observable<Response<String>> getDTPTC(@Path(value = "OrderType") String OrderType, @Path(value = "UserID") String UserID);

    @GET("Masters/AllDivision/{UserID}/{UCID}/{OrderType}")
    Observable<Response<String>> getDivision(@Path(value = "OrderType") String OrderType, @Path(value = "UserID") String UserID, @Path(value = "UCID") String UCID);

    @GET("Masters/GetGroup3/{UserID}/{DivisionID}/{OrderType}")
    Observable<Response<String>> getSubDivisionwithOther(@Path(value = "OrderType") String OrderType, @Path(value = "UserID") String UserID, @Path(value = "DivisionID") String DivisionID);

    @GET("Masters/GetGroup6/{SubDivisionID}/{OrderType}/{SOPOEDate}")
    Observable<Response<String>> getAllCommissionAgent(@Path(value = "OrderType") String OrderType, @Path(value = "SOPOEDate") String SOPOEDate, @Path(value = "SubDivisionID") String SubDivisionID);

    @GET("Masters/AllCountry")
    Observable<Response<String>> getCountryList();

    @Headers("Content-Type:application/x-www-form-urlencoded")
    @FormUrlEncoded
    @POST("Masters/NewCustomerSupplier")
    Observable<Response<String>> AddNewSuppCust(@FieldMap Map<String, String> stringMap);

    @Headers("Content-Type:application/json")
    @POST("users/ChangePassword")
    Observable<Response<String>> ChangePassword(@Body String stringMap);

}
